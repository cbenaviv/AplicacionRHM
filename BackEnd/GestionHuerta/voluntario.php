<?php

header('Content-type: application/json');
header("Access-Control-Allow-Origin: *");

 class voluntario
{

    protected $nombre;
    protected $apellido;
    protected $correo;
    protected $telefono;
    protected $huerta;
    protected $genero;
        



    public function setGenero($genero)
    {
       $this->genero=$genero;
    }


    public function getGenero()
    {
        return $this->genero;
    }

    public function setHuerta($huerta)
    {
       $this->huerta=$huerta;
    }

    public function getHuerta()
    {
        return $this->huerta;
    }

    public function setNombre($nombre)
    {
        $this->nombre=$nombre;

    }

    public function setApellido($apellido)
    {
        $this->apellido=$apellido;
    }


    public function setCorreo($correo)
    {
       $this->correo=$correo;
    }



    public function setTelefono($telefono)
    {
        $this->telefono=$telefono;
    }


    public function getNombre()
    {
        return $this->nombre;

    }


    public function getApellido()
    {
      return $this->apellido;
    }

    public function getCorreo()
    {
        return $this->correo;
    }

    public function getTelefono()
    {
        return $this->telefono;
    }
    

  

}


?>