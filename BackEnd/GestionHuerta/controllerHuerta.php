<?php

header('Content-type: application/json');
header("Access-Control-Allow-Origin: *");

include_once 'huerta.php';
include_once 'voluntario.php';
include_once 'DAOHuerta.php';
include_once 'DAOVoluntario.php';
include_once '../GestionEntrevista/DAOEntrevista.php';

#Se recupera Json

$FileImput = file_get_contents('php://input');
$FileConverted = array();
$FileConverted = json_decode($FileImput);
$opcion=$FileConverted[0]->operacion;



switch($opcion)
{

  case 0:
  #Inserte una Nueva Huerta
  $DAOHuerta=new DAOHuerta();
  $huerta=new huerta();
  $huerta->setNombreHuerta($FileConverted[0]->NombreHuerta);
  $huerta->setUbicacion($FileConverted[0]->Ubicacion);
  $huerta->setArea($FileConverted[0]->AreaHuerta);
  $huerta->setFoto($FileConverted[0]->fotoHuerta);
  $DAOHuerta->setHuerta($huerta); 
  echo $DAOHuerta->insertarHuerta();
  break;


  case 1:
  #listar Todos las huertas
  $DAOHuerta=new DAOHuerta();
  echo $DAOHuerta->listarHuertas();
  break;


  case 2:
  #listar una huerta en particular
  
  $DAOHuerta=new DAOHuerta();
  echo $DAOHuerta->buscarHuerta($FileConverted[0]->CodigoHuerta);
  break;


  case 3:
  #Agregar Miembro
  $voluntario=new Voluntario();
  $voluntario->setNombre($FileConverted[0]->Nombres);
  $voluntario->setApellido($FileConverted[0]->Apellidos);
  $voluntario->setGenero($FileConverted[0]->Genero);
  $voluntario->setCorreo($FileConverted[0]->Correo);
  $voluntario->setTelefono($FileConverted[0]->Telefono);
  $rol=$FileConverted[0]->Rol;

  $DAOVoluntario=new DAOVoluntario();
  $DAOVoluntario->setVoluntario($voluntario);
  echo $DAOVoluntario->insertarVoluntario($FileConverted[0]->CodigoHuerta,$rol);
  break;


  case 4:
  #listar Miembros
  $DAOVoluntario=new DAOVoluntario();
  echo $DAOVoluntario->cargarVoluntarios($FileConverted[0]->CodigoHuerta);
  break;
  
  case 5:
  #listar Version
  $DAOEntrevista=new DAOEntrevista();
  echo $DAOEntrevista->recuperarVersionEntrevista($FileConverted[0]->CodigoHuerta);
  break;




 

}







?>