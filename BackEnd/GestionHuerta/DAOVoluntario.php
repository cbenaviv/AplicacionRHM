<?php

header('Content-type: application/json');
header("Access-Control-Allow-Origin: *");

include_once '../BaseDatos/dbConexion.php';
 class DAOVoluntario

#Realizar Vista Externa

{

    private $dbConexion;
    private $voluntario;
    private $DaoHuerta;
    private $CodigoHuerta;


    public function  DAOVoluntario()
    {
        $this->dbConexion=new database();
      
    }

    public function setDaoHuerta($DaoHuerta)
    {
      $this->DaoHuerta=$DaoHuerta;
    }

    public function getDaoHuerta()
    {
        return $this->DaoHuerta;
    }


    public function  setVoluntario($voluntario)
    {
        $this->voluntario=$voluntario;
    }


    public function insertarVoluntario($CodigoHuerta,$rol)
    {
        $this->CodigoHuerta=$CodigoHuerta;
        $query='insert into miembro  (Nombres,Apellidos,Genero,Correo,Telefono,Rol) 
        values(
        "'.$this->voluntario->getNombre().'",
        "'.$this->voluntario->getApellido().'",
        "'.$this->voluntario->getGenero().'",
        "'.$this->voluntario->getCorreo().'",
        "'.$this->voluntario->getTelefono().'",
        "'.$rol.'")';
        
        $this->dbConexion->conectar();
        $resultado=$this->dbConexion->consulta($query);          
        return $this->verificarCargaVoluntario();



    }

    public function verificarCargaVoluntario()
    {

        $query='select * from miembro 
        where miembro.Nombres="'.$this->voluntario->getNombre().'"and
        miembro.Apellidos="'.$this->voluntario->getApellido().'" and
        miembro.Genero="'.$this->voluntario->getGenero().'" and
        miembro.Correo="'.$this->voluntario->getCorreo().'" and
        miembro.Telefono="'.$this->voluntario->getTelefono().'"';     
       
        $resultado=$this->dbConexion->consulta($query);
        $reg= $this->dbConexion->numero_de_filas($resultado);
        $request=mysql_fetch_row($resultado);

        if($reg==0)
        {
            $voluntario_data=Array(
                'Respuesta'=>'Fallo',
                'nombre'=>$this->voluntario->getNombre(),
                'apellido'=>$this->voluntario->getApellido(),
                'correo'=>$this->voluntario->getCorreo(),
                'telefono'=>$this->voluntario->getTelefono()
            );
            
            return  $cad=json_encode ($voluntario_data);

        }
        else
        {
            return $this->asociarVoluntarioHuerta();
        }


        





    }



    public function asociarVoluntarioHuerta()
    {
      
        $idHuerta=$this->CodigoHuerta;
        $idMiembro=$this->RecuperarIdVolunario();        
        $query='insert into huertapormiembro (Huerta_idHuerta,Miembro_idMiembro)
        values(
        '.$idHuerta.',
        '.$idMiembro.'
        )';

       
        $this->dbConexion->consulta($query);
        return $this->VerificarInsercionVoluntarioHuerta();
     


    }

    public function VerificarInsercionVoluntarioHuerta()
    {

        $query='select miembro.idMiembro,miembro.Nombres,miembro.Apellidos,miembro.Genero,miembro.Correo,miembro.Telefono, huerta.NombreHuerta from miembro inner join huertapormiembro on miembro.idMiembro=huertapormiembro.Miembro_idMiembro inner join huerta on huertapormiembro.Huerta_idHuerta=huerta.idHuerta
        where miembro.Nombres="'.$this->voluntario->getNombre().'" and
        miembro.Apellidos="'.$this->voluntario->getApellido().'" and
        miembro.Genero="'.$this->voluntario->getGenero().'" and
        miembro.Correo="'.$this->voluntario->getCorreo().'" and
        miembro.Telefono="'.$this->voluntario->getTelefono().'" and
        huerta.idHuerta='.$this->CodigoHuerta.'';
        //$this->dbConexion->conectar();
        $resultado=$this->dbConexion->consulta($query);
        $numeroFilas=$this->dbConexion->numero_de_filas($resultado);
        $voluntario_data=Array();
        $reg=mysql_fetch_row($resultado);


        if($numeroFilas==0)
        {
            $voluntario_data[]=Array(
                'Respuesta'=>'Fallo',
                'nombre'=>$this->voluntario->getNombre(),
                'apellido'=>$this->voluntario->getApellido(),
                'Genero'=>$this->voluntario->getGenero(),
                'correo'=>$this->voluntario->getCorreo(),
                'telefono'=>$this->voluntario->getTelefono()
            );
        }
        else
        {   
            $voluntario_data[]=array(
                'Respuesta'=>'Exitosa',
                'Voluntario'=>array(   
    
                    'Nombres'=>$this->voluntario->getNombre(),
                    'Apellidos'=>$this->voluntario->getApellido(),
                    'Correo'=>$this->voluntario->getCorreo(),
                    'Telefono'=>$this->voluntario->getTelefono(),
                
                ),
                'CodigoHuerta'=>$this->CodigoHuerta,
                'CodigoVoluntario'=>$reg['0']            
    
    
                );
        }


      
        return  $cad=json_encode ($voluntario_data);
     
        

    }



    public function cargarVoluntarios($codigoHuerta)
    {
       

       
        $query='select miembro.Nombres,miembro.Apellidos,miembro.Genero,miembro.Correo,miembro.Telefono,huerta.idHuerta,huerta.NombreHuerta,miembro.idMiembro from miembro inner join huertapormiembro on miembro.idMiembro=huertapormiembro.Miembro_idMiembro inner join huerta on huertapormiembro.Huerta_idHuerta=huerta.idHuerta
        where huerta.idHuerta='.$codigoHuerta.'';

        $this->dbConexion->conectar();
        $resultado=$this->dbConexion->consulta($query);
        $numeroFilas=$this->dbConexion->numero_de_filas($resultado);
        $voluntario_data=Array();


        if($numeroFilas!=0)
        {
          
          #Existen Investigadores
            
        while ($reg=mysql_fetch_row($resultado))
        {
          $voluntario_data[]=Array(
              'nombres'=> $reg['0'],
              'apellidos'=> $reg['1'],
              'Genero'=> $reg['2'],
              'Correo'=>$reg['3'],
              'Telefono'=>$reg['4'],
              'CodigoHuerta'=>$reg['5'],
              'NombreHuerta'=>$reg['6'],
              'CodigoMiembro'=>$reg['7']
              );
        }
  


        }

        $this->dbConexion->disconnect();
        return  $cad=json_encode ($voluntario_data);
     

    }


    public function RecuperarIdVolunario()
    {
        $query='select idMiembro from miembro 
        where miembro.Nombres="'.$this->voluntario->getNombre().'"and
        miembro.Apellidos="'.$this->voluntario->getApellido().'" and
        miembro.Correo="'.$this->voluntario->getCorreo().'" and
        miembro.Telefono="'.$this->voluntario->getTelefono().'"';

        $this->dbConexion->conectar();
        $resultado=$this->dbConexion->consulta($query);
        
        $reg=mysql_fetch_row($resultado);
        $this->dbConexion->disconnect();
        return $reg['0'];


    }







}






?>