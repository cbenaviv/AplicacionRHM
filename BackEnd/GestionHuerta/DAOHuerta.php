<?php

header('Content-type: application/json');
header("Access-Control-Allow-Origin: *");

include_once '../BaseDatos/dbConexion.php';
include_once 'huerta.php';

class DAOHuerta
{
  


    private $dbConexion;
    private $huerta;

    public function  DAOHuerta()
    {
        $this->dbConexion=new database();

    }

    public function setHuerta($huerta)
    {
       $this->huerta=$huerta;
    }

    public function getHuerta()
    {
        return $this->huerta;
    }

  public function insertarHuerta()
  {
      $query='insert into huerta (NombreHuerta,Ubicacion,AreaHuerta,foto)
      values(
      "'.$this->huerta->getNombreHuerta().'",
      "'.$this->huerta->getUbicacion().'",
      '.$this->huerta->getArea().',
      "'.$this->huerta->getFoto().'"
      ) ';
      $this->dbConexion->conectar();
      $this->dbConexion->consulta($query);
      return $this->verificarInsercion();
    

  }

  public function verificarInsercion()
  {
      $query='select * from huerta 
      where huerta.NombreHuerta="'.$this->huerta->getNombreHuerta().'" and
      huerta.Ubicacion="'.$this->huerta->getUbicacion().'" and
      huerta.AreaHuerta="'.$this->huerta->getArea().'" and
      huerta.foto="'.$this->huerta->getFoto().'"';

      $resultado=$this->dbConexion->consulta($query);
      $numeroRegistro=$this->dbConexion->numero_de_filas($resultado);
      $huerta_data=Array();

      if($numeroRegistro==0)
      {
          
        $huerta_data[]=array(
            'Resultado'=>'Fallo',
             'NombreHuerta'=>''.$this->huerta->getNombreHuerta().'',
             'UbicacionHuerta'=> ''.$this->huerta->getUbicacion().'',
             'AreaHuerta'=>''.$this->huerta->getArea().'',
             'foto'=>''.$this->huerta->getFoto().''
            
          );
      }
      else
      {
       
        $huerta_data[]=array(
            'Resultado'=>'Exito',
             'NombreHuerta'=>''.$this->huerta->getNombreHuerta().'',
             'UbicacionHuerta'=> ''.$this->huerta->getUbicacion().'',
             'AreaHuerta'=>''.$this->huerta->getArea().'',
             'foto'=>''.$this->huerta->getFoto().''
          );



      }


      return $cad=json_encode ($huerta_data);


  }





  public function listarHuertas()
  {
      $query='select * from huerta';
      $this->dbConexion->conectar();
      $respuesta=$this->dbConexion->consulta($query); 
      $huerta_data=Array();

      while ($reg=mysql_fetch_row($respuesta))
      {
        $huerta_data[]=array(
              'Resultado'=>'Exitoso',
              'CodigoHuerta'=>$reg['0'],
              'NombresHuera'=>$reg['1'],
              'UbicacionHuerta'=>$reg['2'],
              'AreaHuerta'=>$reg['3'],
              'foto'=>$reg['4']

             );
      }

      $this->dbConexion->disconnect();
      return $cad=json_encode ($huerta_data);



  }


  public function actualizarHuerta()
  {
      $query='update huerta set
      NombreHuerta="'.$this->huerta->getNombreHuerta().'",
      ubicacion="'.$this->huerta->getNombreHuerta().'",
      AreaHuerta='.$this->huerta->getArea().',
      foto="'.$this->huerta->getFoto().'"  
      where idHuerta='.$this->recuperarIdHuerta().'    
    ';

    $this->dbConexion->conectar();
     $resultado=$this->dbConexion->consulta($query);
     $huerta_data=Array();
    
     if(mysql_fetch_row($resultado)==0)
     {
         $huerta_data[]=array(
           'Resultado'=>'NoActualizado',
           'NombreHuerta'=>''.$this->huerta->getNombreHuerta().'',
           'Ubicacion'=>''.$this->huerta->getNombreHuerta().'',
           'AreaHuerta'=>''.$this->huerta->getArea().'',
           'foto'=>''.$this->huerta->getFoto().''
         );
     }

     else{

        $huerta_data[]=array(
            'Resultado'=>'Actualizado',
            'NombreHuerta'=>''.$this->huerta->getNombreHuerta().'',
            'Ubicacion'=>''.$this->huerta->getUbicacion().'',
            'AreaHuerta'=>''.$this->huerta->getArea().'',
            'foto'=>''.$this->huerta->getFoto().''
          );


     }


     $this->dbConexion->disconnect();
     $cad=json_encode ($huerta_data);

     

      
      
  }

  public function recuperarIdHuerta()
  {
          
    $query='select idHuerta from huerta 
    where NombreHuerta="'.$this->huerta->getUbicacion().'" and
    Ubicacion="'.$this->huerta->getUbicacion().'"';

    $this->dbConexion->conectar();
    $resultado=$this->dbConexion->consulta($query);
    $req=mysql_fetch_row($resultado);
    $this->dbConexion->disconnect();
    return $req['1'];  


}

  public function buscarHuerta($CodigoHuerta)
  {
    $query='select * from huerta
    where idHuerta='.$CodigoHuerta.'';

    $this->dbConexion->conectar();
    $resultado=$this->dbConexion->consulta($query);
    $numeroRegistro=$this->dbConexion->numero_de_filas($resultado);
    $huerta_data=array();

   
    if($numeroRegistro==0)
    {
        
       $huerta_data[]=array(
           'Resultado'=>'Fallo',
            
         );   
    }
    else
    {

       $req=mysql_fetch_row($resultado);
       
           $huerta_data[]=array(
           'Resultado'=>'Exito',
            'Codigo'=>''.$req['0'].'',
           'NombreHuerta'=>''.$req['1'].'',
           'UbicacionHuerta'=> ''.$req['2'].'',
           'AreaHuerta'=>''.$req['3'].'',
           'foto'=>''.$req['4'].'',
           'CodAux'=>$CodigoHuerta
           );
       
         



    }


    $this->dbConexion->disconnect();
    $cad=json_encode ($huerta_data);

    return $cad;



  }






}




?>