<?php

include_once 'DAORespuesta.php';

header('Content-type: application/json');
header("Access-Control-Allow-Origin: *");


$FileImput = file_get_contents('php://input');
$FileConverted = array();
$FileConverted = json_decode($FileImput);
$DAORespuesta=new DAORespuesta();
$opcion=$FileConverted[0]->operacion;

switch($opcion)
{


   case 1:
   #Verifica si hay respuesta generada
   $idPregunta=$FileConverted[0]->idPregunta;
   $idEntrevista=$FileConverted[0]->idEntrevista;
   $respuesta=$DAORespuesta->verificarRespuesta($idPregunta,$idEntrevista);
   if($respuesta)
   {
     #ExisteRespuesta Generada
	 echo $DAORespuesta->RecuperaRespuesta($idPregunta,$idEntrevista);
	 
   }
   else
   {
    
    echo $DAORespuesta->RecupereFormato($idPregunta,$idEntrevista);  
   }
   
   break;
   
   
    
   
   case 4:
   #inserteRepuestaAbierta
   $contenidoRespuesta=$FileConverted[0]->contenidoRespuesta;
   $codigoPregunta=$FileConverted[0]->codigoPregunta;
   $codigoEntrevista=$FileConverted[0]->codigoEntrevista;
   $idPreguntaPorEntrevista=$FileConverted[0]->idPreguntaPorEntrevista;
   $idOpcionRespuesta=$FileConverted[0]->idOpcionRespuesta;
  
  
   echo $DAORespuesta>insertarRespuestaAbierta($idOpcionRespuesta,$idPreguntaPorEntrevista,$contenidoRespuesta,$codigoPregunta,$codigoEntrevista);
	
   break;
   
   case 5:
   
   #inserte Respuesta Cerrada unica
   $codigoPregunta=$FileConverted[0]->codigoPregunta;
   $codigoEntrevista=$FileConverted[0]->codigoEntrevista;
   $idPreguntaPorEntrevista=$FileConverted[0]->idPreguntaPorEntrevista;
   $idOpcionRespuesta=$FileConverted[0]->idOpcionRespuesta;
   
   echo $DAORespuesta->insertarRespuestaCerradaUnica($idOpcionRespuesta,$idPreguntaPorEntrevista,$codigoPregunta,$codigoEntrevista);
  
       
   break;
   
   
   
    case 6:
    #inserte Respuesta Cerrada Multiple
    
    $codigoPregunta=$FileConverted[0]->codigoPregunta;
    $codigoEntrevista=$FileConverted[0]->codigoEntrevista;
    $idPreguntaPorEntrevista=$FileConverted[0]->idPreguntaPorEntrevista;
    $listado=$FileConverted[0]->listado;//JSON 
    $numeroRegistros=$FileConverted[0]->numeroRegistros;
    
    foreach($FileConverted[0]->listado as $valor)
    {
     $opcionRespuesta=$valor[0]->opcionRespuesta;
     $DAORespuesta->insertarRespuestaCerradaUnica($opcionRespuesta,$idPreguntaPorEntrevista,$codigoPregunta,$codigoEntrevista);
     
    }
    
    echo $DAORespuesta->verificarIngresoMultiple($idPreguntaPorEntrevista,$codigoPregunta,$numeroRegistros);
    
    
   break;
   
   
   
   case 7:
    #Actualice Respuesta Abierta
    $idOpcionRespuesta=$FileConverted[0]->idOpcionRespuesta;
    $idPreguntaPorEntrevista=$FileConverted[0]->idPreguntaPorEntrevista;
    $contenidoRespuesta=$FileConverted[0]->contenidoRespuesta;
    echo $DAORespuesta->ActualizarRespuesta($idOpcionRespuesta,$idPreguntaPorEntrevista,$contenidoRespuesta);
    
   break;
   
    
   case 10:
    #Listar Opciones Respuesta
   $CodigoPregunta=$FileConverted[0]->CodigoPregunta;
   $codEntrevista=$FileConverted[0]->CodigoEntrevista;
   echo $DAORespuesta->listaopcionesRespuestasDefault($CodigoPregunta,$codEntrevista);
   break;
   
    case 11:
   #Actualizar respuestaRegistrada Abierta
   
    $idOpcionRespuesta=$FileConverted[0]->idOpcionRespuesta;
    $idPreguntaPorEntrevista=$FileConverted[0]->idPreguntaPorEntrevista;
    $contenidoRespuesta=$FileConverted[0]->contenido;
    $codPregunta=$FileConverted[0]->codPregunta;
    $DAORespuesta->ActualizarRespuestaAbierta($idOpcionRespuesta,$idPreguntaPorEntrevista,$contenidoRespuesta,$codPregunta);
    break;
   
   case 12:
       
    #Actualizar respuesta Cerrada Unica Con Otro
    $idOpcionRespuesta=$FileConverted[0]->idOpcionRespuesta;
    $idPreguntaPorEntrevista=$FileConverted[0]->idPreguntaPorEntrevista;
    $contenidoRespuesta=$FileConverted[0]->ContenidoOtro;
    $codPregunta=$FileConverted[0]->codPregunta;
    $DAORespuesta->ActualizarRespuestaCerradaConOtro($idOpcionRespuesta,$idPreguntaPorEntrevista,$contenidoRespuesta,$codPregunta);
     break;
   
   
   case 13:
       #Actualizar respuesta cerrada Unica Sin otro
       
      $idOpcionRespuesta=$FileConverted[0]->idOpcionRespuesta;
      $idPreguntaPorEntrevista=$FileConverted[0]->idPreguntaPorEntrevista;
      $codPregunta=$FileConverted[0]->codPregunta;
      $DAORespuesta->ActualizarRespuestaCerradaUnicaSinOtro($idOpcionRespuesta,$idPreguntaPorEntrevista,$codPregunta);
      break;
    
    case 14:
        #Actualizar Respuesta Cerrada Multiple con otro
       $listadoSeleccionado=$FileConverted[0]->ListaOption;
       $idPreguntaPorEntrevista=$FileConverted[0]->idPreguntaPorEntrevista;
       $codPregunta=$FileConverted[0]->codPregunta;
       $contenidoRespuesta=$FileConverted[0]->contenido;
       $idOpcioOtro=$FileConverted[0]->idOpcioOtro;//Id que identifica cual opcion Otro. El codigo en cuestion
       $DAORespuesta->ActualizarRespuestaMultipleConOtro($listadoSeleccionado,$idPreguntaPorEntrevista,$codPregunta,$idOpcioOtro,$contenidoRespuesta);
    break;
   
   
   case 15:
       #Actualizar Respuesta Cerrada Multiple sin otro
       $listadoSeleccionado=$FileConverted[0]->listaOpciones; //Es Necesario Recorrer Este Registro
       $idPreguntaPorEntrevista=$FileConverted[0]->idPreguntaPorEntrevista;
       $codPregunta=$FileConverted[0]->codPregunta;
       $DAORespuesta->ActualizarRespuestaMultipleSinOtro($listadoSeleccionado,$idPreguntaPorEntrevista,$codPregunta);
       
   break;
  
   
  
   

}





?>