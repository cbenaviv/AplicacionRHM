<?php

header('Content-type: application/json');
header("Access-Control-Allow-Origin: *");

include_once '../BaseDatos/dbConexion.php';


class DAORespuesta
{

   private $dbConexion;
   private $respuesta;



   public function DAORespuesta()
   {

     $this->dbConexion=new database();    


   }

   public function setRespuesta($respuesta)
   {
      $this->respuesta=$respuesta;
   }

   public function getRespuesta()
   {
       return $this->respuesta;
   }
   
   public function listarRespuestaAbierta()
   {
      $query='select pregunta.TituloPregunta,pregunta.TipoPregunta,categoria.nombreCategoria,
      opcionrespuesta.DescripcionOpcionRespuesta from pregunta inner join categoria
      on pregunta.categoria_idcategoria=categoria.idcategoria inner join opcionrespuesta
      on pregunta.idPregunta=opcionrespuesta.idOpcionRespuesta
      where pregunta.TituloPregunta="'.$this->respuesta->getNombrePregunta().'"and
      categoria.nombreCategoria="'.$this->respuesta->getPregunta->getCategoria().'"and
      pregunta.TipoPregunta="'.$this->respuesta->getPregunta->getTipo().'"';


      $this->dbConexion->conectar();
      $resultado=$this->dbConexion->consulta($query);
      $opcionRespuesta_data=array();

      $numeroFilas=$this->dbConexion->numero_de_filas( $resultado);
      if($numeroFilas!=0)
      {
        $res=mysql_fetch_row($resultado);
        $opcionRespuesta_data[]=array(
        
            'Pregunta'=>$reg['1'],
            'TipoPregunta'=>$reg['2'],
            'CategoriaPregunta'=>$reg['3'],
            'DescripcionRespuesta'=>$reg['4']



        );

       

      }


      $this->dbConexion->disconnect();
      $cad=json_encode ($opcionRespuesta_data);
      return $cad;     
          
      }


   public function listarRespuestaSeleccion()
   {

    $query='select pregunta.TituloPregunta,pregunta.TipoPregunta,categoria.nombreCategoria,
    opcionrespuesta.DescripcionOpcionRespuesta from pregunta inner join categoria
    on pregunta.categoria_idcategoria=categoria.idcategoria inner join opcionrespuesta
    on pregunta.idPregunta=opcionrespuesta.idOpcionRespuesta
    where pregunta.TituloPregunta="'.$this->respuesta->getNombrePregunta().'"and
    categoria.nombreCategoria="'.$this->respuesta->getPregunta->getCategoria().'"and
    pregunta.TipoPregunta="'.$this->respuesta->getPregunta->getTipo().'"';

   
    $this->dbConexion->conectar();
    $resultado=$this->dbConexion->consulta($query);
    $numeroFilas=$this->dbConexion->numero_de_filas( $resultado);
    $opcionRespuesta_data=array();

    if($numeroFilas!=0)
    {
        while($res=mysql_fetch_row($resultado))
        {
  
            $opcionRespuesta_data[]=array(
        
                'Pregunta'=>$reg['1'],
                'TipoPregunta'=>$reg['2'],
                'CategoriaPregunta'=>$reg['3'],
                'DescripcionRespuesta'=>$reg['4']
    
    
    
            );



        }

    }

    $this->dbConexion->disconnect();
    $cad=json_encode ($opcionRespuesta_data);
    return $cad; 




   }
   
   
   public function verificarRespuesta($idPregunta,$idEntrevista)
   {  
      
	  
	  $query='select * from respuestaregistrada
	  inner join preguntaporentrevista on respuestaregistrada.preguntaporentrevista_idPreguntaPorEntrevista=preguntaporentrevista.idPreguntaPorEntrevista
	  inner join entrevista on preguntaporentrevista.Entrevista_idEntrevista=entrevista.idEntrevista 
	  inner join pregunta on preguntaporentrevista.Pregunta_idPregunta=pregunta.idPregunta 
	  where pregunta.idPregunta='.$idPregunta.' and entrevista.idEntrevista='.$idEntrevista.'';
	  
	  
	  $this->dbConexion->conectar();
      $resultado=$this->dbConexion->consulta($query);
	  $numeroFilas=$this->dbConexion->numero_de_filas( $resultado);
	
	  
	  if($numeroFilas!=0)
	  { 
	  
	     return true;
        
	  }
	  
	  
	  return false;
	  
	  	 
	 }
	 
	 public function RecuperaRespuesta($CodPregunta,$entrevista)
	 {
	        
         //Cuando Existe Respuesta Registrada
		 
		 $query='select opcionrespuesta.idOpcionRespuesta,opcionrespuesta.DescripcionOpcionRespuesta,preguntaporentrevista.Pregunta_idPregunta,respuestaregistrada.contenido, respuestaregistrada.idRespuestaRegistrada from opcionrespuesta inner join respuestaregistrada on opcionrespuesta.idOpcionRespuesta=respuestaregistrada.OpcionRespuesta_idOpcionRespuesta inner join preguntaporentrevista ON respuestaregistrada.preguntaporentrevista_idPreguntaPorEntrevista=preguntaporentrevista.idPreguntaPorEntrevista where 
		  respuestaregistrada.pregunta='.$CodPregunta.' and 
		  preguntaporentrevista.Entrevista_idEntrevista='.$entrevista.' ';
		 
		 
		  $this->dbConexion->conectar();
		  $resultado=$this->dbConexion->consulta($query);
	      $numeroFilas=$this->dbConexion->numero_de_filas( $resultado);
		  $opcionRespuesta_data=array();
		
		  
		  if($numeroFilas==0)
		  {
		      $opcionRespuesta_data[]=array(
			   "Respuesta"=>"fallo");
		  }
		  
		  else
		  {    
		        $listado_data=array();
			   while($res=mysql_fetch_row($resultado))
			  {
			   
			     $listado_data[]=array(
				 
				  "idOpcionRespuesta"=>$res['0'],
			      "DescripcionOpcionRespuesta"=>$res['3'],
			      "ContenidoAuxiliar"=>$res['1']
			   
				 );
			   
			  
               }
			  
			  
			  
			  $opcionRespuesta_data[]=array(
		       "Respuesta"=>"true",
			   "idEntrevista"=>$entrevista,
               "idPregunta"=>$CodPregunta,
               "TipoPregunta"=>$this->detallePregunta($CodPregunta),
               "OpcionRespuesta"=>$this->opcionRespuesta($CodPregunta),
			   "idPreguntaPorEntrevista"=>$this->recupereIdPreguntaPorEntrevista($entrevista,$CodPregunta),
			   "listado"=>$listado_data
			   );
			  
			
			
		  }
		  
		   $cad=json_encode ($opcionRespuesta_data);
           return  $cad;
	     
	   
	 }
	 
	 
	 
	 public function RecupereFormato($idPregunta,$idEntrevista)
	 {
	
        //Formato
		   
		 
		   $query='select * from opcionrespuesta inner join pregunta on pregunta.idPregunta=opcionrespuesta.Pregunta_idPregunta 
		   inner join preguntaporentrevista on pregunta.idPregunta=preguntaporentrevista.Pregunta_idPregunta 
		   where preguntaporentrevista.Pregunta_idPregunta='.$idPregunta.' and preguntaporentrevista.Entrevista_idEntrevista='.$idEntrevista.'';
		   
		  $this->dbConexion->conectar();
		  $resultado=$this->dbConexion->consulta($query);
	      $opcionRespuesta_data=array();
	      
	          $listado_data=array();
			   while($res=mysql_fetch_row($resultado))
			  {
			   
			     $listado_data[]=array(
				 
				  "idOpcionRespuesta"=>$res['1'],
			      "DescripcionOpcionRespuesta"=>$res['2']
			   
				 );
			   
			  
               }
	       
	       
	    
		   $opcionRespuesta_data[]=array(
		       "Respuesta"=>"false",
		       "idEntrevista"=>$idEntrevista,
               "idPregunta"=>$idPregunta,
			   "TipoPregunta"=>$this->detallePregunta($idPregunta),
			   "OpcionRespuesta"=>$this->opcionRespuesta($idPregunta),
			    "idPreguntaPorEntrevista"=>$this->recupereIdPreguntaPorEntrevista($idEntrevista,$idPregunta),
			   "listado"=>$listado_data
			 
			   );
			   
		   $cad=json_encode ($opcionRespuesta_data);
           return $cad; 
		   
	 
	     
	 
	 }
	 
	 
	 public function recupereIdPreguntaPorEntrevista($codigoEntrevista,$codigoPregunta)
	 {
	     $query='select preguntaporentrevista.idPreguntaPorEntrevista,preguntaporentrevista.Entrevista_idEntrevista from preguntaporentrevista inner join 
	          pregunta on preguntaporentrevista.Pregunta_idPregunta=pregunta.idPregunta 
	          inner join entrevista ON preguntaporentrevista.Entrevista_idEntrevista=entrevista.idEntrevista
	          where preguntaporentrevista.Entrevista_idEntrevista='.$codigoEntrevista.'
	          and preguntaporentrevista.Pregunta_idPregunta='.$codigoPregunta.'';
	          
	   $resultado=$this->dbConexion->consulta($query);
	   $res=mysql_fetch_row($resultado);
	   return $res['0'];
	          
	          
	 }
	 
	 
	 
	 public function detallePregunta($codigoPregunta)
	 {
	 
	   $query='select * from pregunta 
	   inner join opcionrespuesta on 
	   pregunta.idPregunta=opcionrespuesta.Pregunta_idPregunta
	   where  pregunta.idPregunta='.$codigoPregunta.'';
	   
	   $resultado=$this->dbConexion->consulta($query);
	   $res=mysql_fetch_row($resultado);
	   return $res['2'];
	   
	   
	 }
	 
	 public function opcionRespuesta($codigoPregunta)
	 {
	     $query='select * from pregunta 
	   inner join opcionrespuesta on 
	   pregunta.idPregunta=opcionrespuesta.Pregunta_idPregunta
	   where  pregunta.idPregunta='.$codigoPregunta.'';
	   
	   $resultado=$this->dbConexion->consulta($query);
	   $res=mysql_fetch_row($resultado);
	   return $res['4'];
	   
	   
	 }
	 
	 
	 public function insertarRespuestaAbierta($idOpcionRespuesta,$idPreguntaPorEntrevista,$contendoRespuesta,$codigoPregunta,$codigoEntrevista)
	 {
	   
	     $query='insert into respuestaregistrada (OpcionRespuesta_idOpcionRespuesta,pregunta,contenido,preguntaporentrevista_idPreguntaPorEntrevista) 
	    values ('.$idOpcionRespuesta.','.$codigoPregunta.',"'.$contendoRespuesta.'",'.$idPreguntaPorEntrevista.')';
		
		$this->dbConexion->conectar();
		$resultado=$this->dbConexion->consulta($query);
		return $this->validarIngresoRespuestaAbierta($idOpcionRespuesta,$idPreguntaPorEntrevista,$codigoPregunta);
		
	 }
	 
	 
	 public function validarIngresoRespuestaAbierta($idOpcionRespuesta,$idPreguntaPorEntrevista,$codigoPregunta)
	 {
	      
         $query='select respuestaregistrada.idRespuestaRegistrada from respuestaregistrada inner JOIN opcionrespuesta on respuestaregistrada.OpcionRespuesta_idOpcionRespuesta=opcionrespuesta.idOpcionRespuesta inner join pregunta on respuestaregistrada.pregunta=pregunta.idPregunta inner join preguntaporentrevista on respuestaregistrada.preguntaporentrevista_idPreguntaPorEntrevista=preguntaporentrevista.idPreguntaPorEntrevista where respuestaregistrada.OpcionRespuesta_idOpcionRespuesta='.$idOpcionRespuesta.' and 
	         respuestaregistrada.pregunta='.$codigoPregunta.' and 
	         respuestaregistrada.preguntaporentrevista_idPreguntaPorEntrevista='.$idPreguntaPorEntrevista.'';
	   
         
         $resultado=$this->dbConexion->consulta($query);
	     $numeroFilas=$this->dbConexion->numero_de_filas( $resultado);
	     $validacion_option=array();
	         
	         if($numeroFilas==0)
	         {
	             $validacion_option[]=Array(
	             
	             'respuesta'=>'fallo',
	              'idOpcionRespuesta'=>$idOpcionRespuesta,
	              'idPreguntaPorEntrevista'=>$idPreguntaPorEntrevista,
	              'idPregunta'=>$codigoPregunta
	             
	             );
	         
	         }
	         else
	         {
	             $validacion_option[]=Array(
	             
	             'respuesta'=>'Exito',
	              'idOpcionRespuesta'=>$idOpcionRespuesta,
	              'idPreguntaPorEntrevista'=>$idPreguntaPorEntrevista,
	              'idPregunta'=>$codigoPregunta
	             
	             );
	             
	         }
	         
	         	   
		   $cad=json_encode ($validacion_option);
           return $cad; 
	         
	         
	         
	         
	         
	         
	 }
	 
	 
	 
	 
	 public function verificarRespuestaAbierta($idEntrevista,$codRespuesta)
	 {
	    $query='select * from respuestaregistrada
		inner join opcionrespuesta ON respuestaregistrada.OpcionRespuesta_idOpcionRespuesta=opcionrespuesta.idOpcionRespuesta 
		inner join pregunta on opcionrespuesta.Pregunta_idPregunta=pregunta.idPregunta 
		inner join preguntaporentrevista on pregunta.idPregunta=preguntaporentrevista.Pregunta_idPregunta 
		inner join entrevista on preguntaporentrevista.Entrevista_idEntrevista=entrevista.idEntrevista 
		where entrevista.idEntrevista='.$idEntrevista.' and respuestaregistrada.OpcionRespuesta_idOpcionRespuesta='.$codRespuesta.'';
		
		$resultado=$this->dbConexion->consulta($query);
		$numeroFilas=$this->dbConexion->numero_de_filas( $resultado);
		$opcionRespuesta_data=array();
		
		if($numeroFilas==0)
		{
		    $opcionRespuesta_data[]=array(
			   "Respuesta"=>"fallo"		   
			   
			  );
		}
		else
		{  $opcionRespuesta_data[]=array(
			   "Respuesta"=>"Exito"	,
               "idEntrevista"=>$idEntrevista,
                "codRespuesta"=>$codRespuesta			   
			   
			  );
		}
		
		  $cad=json_encode ($opcionRespuesta_data);
          return $cad; 
		   
		
		
		 }
		 
		 
		 public function insertarRespuestaCerradaUnica($idOpcionRespuesta,$idPreguntaPorEntrevista,$codigoPregunta,$codigoEntrevista)
		 {
		   
			$query='insert into respuestaregistrada 
			(OpcionRespuesta_idOpcionRespuesta,pregunta,preguntaporentrevista_idPreguntaPorEntrevista)
			values ('.$idOpcionRespuesta.','.$codigoPregunta.','.$idPreguntaPorEntrevista.')';
			$this->dbConexion->conectar();
			
			$resultado=$this->dbConexion->consulta($query);
			//En este caso como la pregunta es cerrada con unica respuesta se toma como si fuera una abierta
			return  $this->validarIngresoRespuestaAbierta($idOpcionRespuesta,$idPreguntaPorEntrevista,$codigoPregunta);
		 
		 }
		 
		 
		public function verificarIngresoMultiple($preguntaPorEntrevista,$pregunta,$numeroRegistros)
		{
		    $query='select respuestaregistrada.idRespuestaRegistrada from respuestaregistrada inner join preguntaporentrevista on respuestaregistrada.preguntaporentrevista_idPreguntaPorEntrevista=preguntaporentrevista.idPreguntaPorEntrevista where 
		         respuestaregistrada.pregunta='.$pregunta.' and 
		         respuestaregistrada.preguntaporentrevista_idPreguntaPorEntrevista='.$preguntaPorEntrevista.'';
		    
		    $resultado=$this->dbConexion->consulta($query);
		    $numeroFilas=$this->dbConexion->numero_de_filas($resultado);
		    $opcion_respuesta=Array();
		    
		    if($numeroFilas==$numeroRegistros)
		    {
		        $opcion_respuesta[]=array(
		         
		         "Resultado"=>"Exito"
		            
		       );
		    }
		    else
		    {
		        $opcion_respuesta[]=array(
		         
		         "Resultado"=>"Fallo"
		            
		       );
		        
		    }
		    
		   $cad=json_encode ($opcion_respuesta);
          return $cad; 
		    
		    
		}
		 
		 
		 
		  public function ActualizarRespuestaAbierta($idOpcionRespuesta,$idPreguntaPorEntrevista,$contenidoRespuesta,$codPregunta)
		  
		  {
	       $query='update respuestaregistrada set contenido="'.$contenidoRespuesta.'"
	       where respuestaregistrada.preguntaporentrevista_idPreguntaPorEntrevista='.$idPreguntaPorEntrevista.' and
	       respuestaregistrada.OpcionRespuesta_idOpcionRespuesta='.$idOpcionRespuesta.' and 
	       respuestaregistrada.pregunta='.$codPregunta.'';
	       $this->dbConexion->conectar();
	     
	        $resultado=$this->dbConexion->consulta($query);
	     
	       
	     
	      }
	      
	      
	      public function ActualizarRespuestaCerradaUnicaSinOtro($idOpcionRespuesta,$idPreguntaPorEntrevista,$codPregunta)
	      {
	          
	          $query='update respuestaregistrada set OpcionRespuesta_idOpcionRespuesta='.$idOpcionRespuesta.'
	         where respuestaregistrada.preguntaporentrevista_idPreguntaPorEntrevista='.$idPreguntaPorEntrevista.' and
	         respuestaregistrada.pregunta='.$codPregunta.'';
	         
	          $this->dbConexion->conectar();
		      $resultado=$this->dbConexion->consulta($query);
	      }
	      
	      public function ActualizarRespuestaCerradaConOtro($idOpcionRespuesta,$idPreguntaPorEntrevista,$contenidoRespuesta,$codPregunta)
	      {
	          
	       $this->ActualizarRespuestaCerradaUnicaSinOtro($idOpcionRespuesta,$idPreguntaPorEntrevista,$codPregunta);
	       
	       $query='update respuestaregistrada set contenido="'.$contenidoRespuesta.'"
	       where respuestaregistrada.preguntaporentrevista_idPreguntaPorEntrevista='.$idPreguntaPorEntrevista.' 
	       and  respuestaregistrada.OpcionRespuesta_idOpcionRespuesta='.$idOpcionRespuesta.' and 
	       respuestaregistrada.pregunta='.$codPregunta.'';
	       
	       $this->dbConexion->conectar();
		   $resultado=$this->dbConexion->consulta($query);

	     
	          
	      }
	      
	      
	      
	      
		 
		 
		 	 public function verificarRespuestaTwo($idPreguntaPorEntrevista,$opcionRespuesta)
		 {
			 $query='select * from respuestaregistrada 
			 where respuestaregistrada.OpcionRespuesta_idOpcionRespuesta='.$opcionRespuesta.' and 
			 respuestaregistrada.preguntaporentrevista_idPreguntaPorEntrevista='.$idPreguntaPorEntrevista.'';
			 $resultado=$this->dbConexion->consulta($query);
			 $numeroFilas=$this->dbConexion->numero_de_filas($resultado);
			 return $numeroFilas;

		 }
		 
		 
		 public function verificarTransaccion($opciones_selec,$idPreguntaPorEntrevista,$pregunta)
		 {
			
			$query='select COUNT(respuestaregistrada.idRespuestaRegistrada) 
			 from respuestaregistrada inner join preguntaporentrevista on respuestaregistrada.preguntaporentrevista_idPreguntaPorEntrevista=preguntaporentrevista.idPreguntaPorEntrevista 
			 inner join pregunta on respuestaregistrada.pregunta=pregunta.idPregunta
			  where pregunta.idPregunta='.$pregunta.' and preguntaporentrevista.idPreguntaPorEntrevista='.$idPreguntaPorEntrevista.'';
		 
			  $resultado=$this->dbConexion->consulta($query);
			  $res=mysql_fetch_row($resultado);
			  $numeRegistros=$res;
			  $numeroSeleccion=$this->sizeOpcioneSeleccionadas($opciones_selec);
			  $respuesta_final=array();

			  if($numeRegistros==$numeroSeleccion)
			  {
                  $respuesta_final[]=array(
					"Estado"=>"Exito",
					

				  );
			  }
			  else
			  {
				$respuesta_final[]=array(
					"Estado"=>"Fallo",
					

				  );
			  }

			  $cad=json_encode ($respuesta_final);
			  return $cad; 

			  
			
		}
		
		public function sizeOpcioneSeleccionadas($opciones_selec)
		{
		     $i=0;
		  
		  foreach($opciones_selec as $valor )
		  {
		      $i++;
		  }
		  
		  return $i;

		}
		
		
			public function listaopcionesRespuestasDefault($idPregunta,$idEntrevista)
		{
			$query='select * from opcionrespuesta 
			inner JOIN pregunta on opcionrespuesta.Pregunta_idPregunta=pregunta.idPregunta 
			inner join preguntaporentrevista on pregunta.idPregunta=preguntaporentrevista.Pregunta_idPregunta 
			where pregunta.idPregunta='.$idPregunta.' and 
			preguntaporentrevista.Entrevista_idEntrevista='.$idEntrevista.'
		    group by opcionrespuesta.idOpcionRespuesta';

			$this->dbConexion->conectar();
			$resultado=$this->dbConexion->consulta($query);
			$lista_opcines=array();

			while($res=mysql_fetch_row($resultado))
			{
			   $lista_opcines[]=array(

				  "idOpcionRespuesta"=>$res['0'],
				  "DescripcionRespuesta"=>$res['2']
				  

			   );
		
			}

			$cad=json_encode ($lista_opcines);
			return $cad; 





		}
		
		
		
		
	public function listadoBaseDatos($idPreguntaPorEntrevista,$codPregunta)
  
  {
     //Actualizacion Opciones Multiples 
        
    $query='SELECT * from respuestaregistrada inner join preguntaporentrevista on respuestaregistrada.preguntaporentrevista_idPreguntaPorEntrevista=preguntaporentrevista.idPreguntaPorEntrevista inner join pregunta on respuestaregistrada.pregunta=pregunta.idPregunta inner join opcionrespuesta on respuestaregistrada.OpcionRespuesta_idOpcionRespuesta=opcionrespuesta.idOpcionRespuesta 
    where respuestaregistrada.preguntaporentrevista_idPreguntaPorEntrevista='.$idPreguntaPorEntrevista.'
    and respuestaregistrada.pregunta='.$codPregunta.'';
	 
	 
	  $this->dbConexion->conectar();
      $resultado=$this->dbConexion->consulta($query);  
	  
      
      
      return $resultado;
     
  }
  
  public function verifiqueSeleccionado($listadoSeleccionado,$valPara)
  {  
    foreach($listadoSeleccionado as $valor)
{     
      
      if($valor['0']->opcionRespuesta==$valPara)
      {
          return true;
      }
      
      
}

return false;

  }
  
  public function verificarRegistro($valorPara,$codPregunta,$idPreguntaPorEntrevista)
  {      
          $query='select * from opcionrespuesta 
		 inner JOIN respuestaregistrada on opcionrespuesta.idOpcionRespuesta=respuestaregistrada.OpcionRespuesta_idOpcionRespuesta 
		 where respuestaregistrada.OpcionRespuesta_idOpcionRespuesta='.$valorPara.' and 
		 respuestaregistrada.pregunta='.$codPregunta.' and 
		 respuestaregistrada.preguntaporentrevista_idPreguntaPorEntrevista='.$idPreguntaPorEntrevista.'';
		
		 $resultado=$this->dbConexion->consulta($query);  
		 $numeroFilas=$this->dbConexion->numero_de_filas($resultado);
		 
		 if($numeroFilas==0)
		 {
		     return false;
		 }
		 
		 return true;
		 
          
  }
  
  public function insertNuevoRecordAnswer($idPreguntaPorEntrevista,$codPregunta,$idOpcionRespuesta)
  {
      //Insert Nuevo Record
	 $query='insert into respuestaregistrada (OpcionRespuesta_idOpcionRespuesta,pregunta,preguntaporentrevista_idPreguntaPorEntrevista)
			values 
            ('.$idOpcionRespuesta.','.$codPregunta.','.$idPreguntaPorEntrevista.')';      
      
        $resultado=$this->dbConexion->consulta($query);
			
  }
  
   public function DeleteRecordAnswer($idPreguntaPorEntrevista,$codPregunta,$idOpcionRespuesta)
  {
    //Elimine Record
	  $query='delete from respuestaregistrada WHERE 
              respuestaregistrada.OpcionRespuesta_idOpcionRespuesta='.$idOpcionRespuesta.' 
              and respuestaregistrada.pregunta='.$codPregunta.' 
              and respuestaregistrada.preguntaporentrevista_idPreguntaPorEntrevista='.$idPreguntaPorEntrevista.'';
      
      $resultado=$this->dbConexion->consulta($query);
  }
  
   public function insertNuevoRecordAnswerOtro($idPreguntaPorEntrevista,$codPregunta,$data,$contenido)
  {
     //Inserte
       $query='insert into respuestaregistrada 
	    (OpcionRespuesta_idOpcionRespuesta,pregunta,contenido,preguntaporentrevista_idPreguntaPorEntrevista) 
	    values ('.$data.','.$codPregunta.',"'.$contenido.'",'.$idPreguntaPorEntrevista.')';
        $resultado=$this->dbConexion->consulta($query);
		
	
  }
  
  public function ActualizarContenidoOtro($idPreguntaPorEntrevista,$codPregunta,$data,$contenido)
  {   //Actualice
       $query='update respuestaregistrada set contenido="'.$contenido.'"
	       where respuestaregistrada.preguntaporentrevista_idPreguntaPorEntrevista='.$idPreguntaPorEntrevista.' and
	       respuestaregistrada.OpcionRespuesta_idOpcionRespuesta='.$data.' and
	       respuestaregistrada.pregunta='.$codPregunta.'';      
       
       $resultado=$this->dbConexion->consulta($query);
	     
  }
  
  
  public function ActualizarRespuestaMultipleSinOtro($listadoSeleccionado,$idPreguntaPorEntrevista,$codPregunta)
  {
      //$listaDatabase
      $resultado=$this->listadoBaseDatos($idPreguntaPorEntrevista,$codPregunta);
	  
	  
	   while($resp=mysql_fetch_row($resultado))
	  {
	      
		 $r=$this->verifiqueSeleccionado($listadoSeleccionado,$resp['1']);
		 if($r==false)
		 {
		   //Elimine Record
		    $this->DeleteRecordAnswer($idPreguntaPorEntrevista,$codPregunta,$resp['1']);
		   
		 }
		 
		 
		 
		 
	  }
	  
	 
	  foreach($listadoSeleccionado as $data)
	  {
	       
	       
	       $rw=$this->verificarRegistro($data['0']->opcionRespuesta,$codPregunta,$idPreguntaPorEntrevista);
		   if($rw==false)
		   {
		      //Inserte Base Datos
			  $this->insertNuevoRecordAnswer($idPreguntaPorEntrevista,$codPregunta,$data['0']->opcionRespuesta);
			  
		   }
	  }
	  
	    $res_final=array();
	    
	    $res_final[]=array(
	        
	        "Estado"=>"finalizado"
	        
	        );
	        
	   $cad=json_encode ($res_final);
       echo  $cad; 
	    
	  
	  
	  }
	  
	  public function ActualizarRespuestaMultipleConOtro($listadoSeleccionado,$idPreguntaPorEntrevista,$codPregunta,$idOpcioOtro,$contenido)
	  {
	   
	    $resultado=$this->listadoBaseDatos($idPreguntaPorEntrevista,$codPregunta);
	  
	  
	   while($resp=mysql_fetch_row($resultado))
	  {
	      
		 $r=$this->verifiqueSeleccionado($listadoSeleccionado,$resp['1']);
		 if($r==false)
		 {
		   //Elimine Record
		    $this->DeleteRecordAnswer($idPreguntaPorEntrevista,$codPregunta,$resp['1']);
		   
		 }
		 
		 
		 
		 
	  }
	  
	  
	   foreach($listadoSeleccionado as $data)
	  {
	       
	       
	       $rw=$this->verificarRegistro($data['0']->opcionRespuesta,$codPregunta,$idPreguntaPorEntrevista);
		   if($rw==false)
		   {
		      //Inserte Base Datos
			  $this->insertNuevoRecordAnswer($idPreguntaPorEntrevista,$codPregunta,$data['0']->opcionRespuesta);
			  
		   }
		   
		   
		   if($r==true && $idOpcioOtro==$data['0']->opcionRespuesta)
     { 
         //Actualice Conte
         $this->ActualizarContenidoOtro($idPreguntaPorEntrevista,$codPregunta,$data['0']->opcionRespuesta,$contenido);
     }
		   
	  }
	  
	  
	  $res_final=array();
	    
	    $res_final[]=array(
	        
	        "Estado"=>"finalizado"
	        
	        );
	        
	   $cad=json_encode ($res_final);
       echo  $cad; 
		   
		   
	
	  
		   
		 
		   
		   
	  }
  
  
  
  
  
  
 
 
  
  
  
  
  
  
  
		
		
		
		
		
		
		 
		
		
		
		
		 
		 

	      
	      
	      
	      
	      
	      
	      
	      
	      
	      
	      
	      
	      
	     
	 
		 
		
		 
		 
		
   
   
   
   
   
   


}





?>