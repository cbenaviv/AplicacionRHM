<?php



header('Content-type: application/json');
header("Access-Control-Allow-Origin: *");

include_once 'entrevista.php';
include_once 'investigador.php';
include_once 'DAOEntrevista.php';
include_once 'DAOInvestigador.php';
include_once 'DAOPregunta.php';
include_once '../GestionHuerta/huerta.php';


$FileImput = file_get_contents('php://input');
$FileConverted = array();
$FileConverted = json_decode($FileImput);
//echo $FileConverted[0]->usuario;


#Recibimos el objetos Json


$opcion=$FileConverted[0]->operacion;


$DAOEntrevista=new DAOEntrevista();


switch($opcion)
{

case 0:
#lista Todas las entrevistas
$huerta=new huerta();
$entrevista=new entrevista();
echo $DAOEntrevista->listarEntrevistas();
break;

case 1:
#listar una Entrevista En especial
$codigoEntrevista=$FileConverted[0]->CodigoEntrevista;
echo $DAOEntrevista->listarUnicaEntrevista($codigoEntrevista);
break;

case 2:
#Listar Categorias de Entrevista
echo $DAOEntrevista->listarCatergoriaPreguntas();
break;

case 3:
#Listas Miembros Entrevistados
$codigoEntrevista=$FileConverted[0]->CodigoEntrevista;
echo $DAOEntrevista->listarMiembrosEntrevistados($codigoEntrevista);
break;

case 4:
#Inserte Entrevista
$huerta=$FileConverted[0]->CodigoHuerta;
$CodigoInvestigador=$FileConverted[0]->Investigador;
$entrevista=new Entrevista();
$entrevista->setFechaEntrevista($FileConverted[0]->FechaEntrevista);
$entrevista->setVersion($FileConverted[0]->VersionEntrevista);
$entrevista->setObservaciones($FileConverted[0]->Observaciones);
$DAOEntrevista->setEntrevista($entrevista);
echo $DAOEntrevista->insertarEntrevista($huerta,$CodigoInvestigador);
break;


case 5:
#Ingresar  Entrevistados
$codInterview=$FileConverted[0]->Entrevista;
foreach($FileConverted[0]->listado as $valor)
{
 $codigoEntrevista=$valor[0]->Codigo;
  $codigoMiembro=$valor[0]->Miembro;
  $DAOEntrevista->AgregarEntrevistadostwo($codigoEntrevista,$codigoMiembro);
}

echo $DAOEntrevista->validacion($codInterview);
break;

case 6:
#Listar Preguntas Categoria 
 $codigoCategoria=$FileConverted[0]->CodigoCategoria;
 $CodigoEntrevista=$FileConverted[0]->CodigoEntrevista;

 echo  $DAOEntrevista->listarPreguntaPorCategoria($codigoCategoria,$CodigoEntrevista);
 break;
 
 case 7:
  #insertar preguntas por entrevista
  $codigoEntrevista=$FileConverted[0]->Entrevista;
  $DAOEntrevista->asignaPreguntaPorEntrevista($codigoEntrevista);
  echo $DAOEntrevista->validarOperacionInsercion($codigoEntrevista);
 break;


              



}






?>