<?php


header('Content-type: application/json');
header("Access-Control-Allow-Origin: *");

include_once '../BaseDatos/dbConexion.php';
include_once '../GestionHuerta/DAOHuerta.php';
include_once 'DAOInvestigador.php';
class DAOEntrevista
{


    private $dbConexion;
    private $entrevista;
    private $DaoHuerta;
    private $DaoInvestigador;
    private $idHuerta;
    private $idInvestigador;

    public function  DAOEntrevista()
    {
       $this->dbConexion=new database();    
       $this->DaoHuerta=new DAOHuerta();
       $this->DaoInvestigador=new DaoInvestigador();
    }
  

    public function setEntrevista($entrevista)
    {
          $this->entrevista=$entrevista;

    }

    public function getEntrevista()
    {
        return $this->entrevista;
    }



    public function  insertarEntrevista($idHuerta,$idInvestigador)
    {
        $this->idHuerta=$idHuerta;
        $this->idInvestigador=$idInvestigador;

        $query='insert into entrevista  (Fecha,Observaciones,Version,Huerta_idHuerta,investigador_idInvestigador)
         values(

         "'.$this->entrevista->getFechaEntrevista().'",
         "'.$this->entrevista->getObservaciones().'",
         "'.$this->entrevista->getVersion().'",
         '.$this->idHuerta.',
         '.$this->idInvestigador.')';

        $this->dbConexion->conectar();
        $resultado=$this->dbConexion->consulta($query);

        return $this->verificarIngresoEntrevista();
        

       

    }


    public function  verificarIngresoEntrevista()
    {
        $query='select * from entrevista 
        where entrevista.Fecha="'.$this->entrevista->getFechaEntrevista().'" and
        entrevista.Huerta_idHuerta='.$this->idHuerta.' and
        entrevista.investigador_idInvestigador='.$this->idInvestigador.' and
        entrevista.Version="'.$this->entrevista->getVersion().'"';

        $this->dbConexion->conectar();
        $resultado=$this->dbConexion->consulta($query);
        $numeroFilas=$this->dbConexion->numero_de_filas($resultado);
        $entrevista_data=Array();
        $req=mysql_fetch_row($resultado);

        if($numeroFilas==0)
        {
           #Ingreso Sin Exito
           
           $entrevista_data[]=Array(
            'Resultado'=>'Fallo',
           'FechaEntrevista'=>''.$this->entrevista->getFechaEntrevista().'',
           'Observaciones'=>''.$this->entrevista->getObservaciones().'',
           'Version'=>''.$this->entrevista->getVersion().'',
           'Huerta'=>$this->idHuerta,
           'Investigador'=>$this->idInvestigador
          );

        }
        else
        {
            #Ingreso Con Exito
            
            $entrevista_data[]=Array(

                'Resultado'=>'Exito',
                'FechaEntrevista'=>''.$this->entrevista->getFechaEntrevista().'',
                'Observaciones'=>''.$this->entrevista->getObservaciones().'',
                'Version'=>''.$this->entrevista->getVersion().'',
                'Huerta'=>$this->idHuerta,
                'Investigador'=>$this->idInvestigador,
                'CodigoEntrevista'=>$req['0']

            );



        }


        $this->dbConexion->disconnect();
        $cad=json_encode ($entrevista_data);

        return $cad;
        


    }


    public function listarEntrevistas()
    {
        
         $query='select entrevista.idEntrevista, entrevista.Fecha,entrevista.Observaciones,entrevista.Version, huerta.idHuerta,huerta.NombreHuerta
         from entrevista inner join huerta on entrevista.Huerta_idHuerta=huerta.idHuerta;';

         $this->dbConexion->conectar();
         $resultado=$this->dbConexion->consulta($query);
         $numeroFilas=$this->dbConexion->numero_de_filas($resultado);
         $entrevista_data=Array();
         if($numeroFilas==0)
         {
             $entrevista_data[]=array(

              'Resultado'=>'Empty'


             );
         }
         else
         {    

            while ($reg=mysql_fetch_row($resultado))
            {
                $entrevista_data[]=array(
                    'Resultado'=>'Exitoso',
                    'Codigo'=>''.$reg['0'].'',
                    'FechaEntrevista'=>''.$reg['1'].'',
                    'Observaciones'=>''.$reg['2'].'',
                    'Version'=>''.$reg['3'].'',
                    'idHuerta'=>''.$reg['4'].'',
                    'NombreHuerta'=>''.$reg['5'].''
   
    
                   );
            }

              

         }


         
        $this->dbConexion->disconnect();
        $cad=json_encode ($entrevista_data);
        return $cad;


    }


    public function listarMiembrosEntrevistados($idEntrevista)
    {
        #Listado los miembros que respondieron la entrevista
       
        $query="select Nombres,Apellidos,Genero,Correo,Telefono from entrevista inner join entrevistapormiembro on entrevista.idEntrevista=entrevistapormiembro.entrevista_idEntrevista inner join miembro on entrevistapormiembro.miembro_idMiembro=miembro.idMiembro
        where entrevista.idEntrevista=".$idEntrevista."";
    

        $this->dbConexion->conectar();
        $resultado=$this->dbConexion->consulta($query);
        $numeroFilas=$this->dbConexion->numero_de_filas($resultado);
        $miembros_data=Array();

        if($numeroFilas==0)
        {
              $miembros_data[]=array(
                      'Resultado'=>'Empty'

              );
        }
        else
        {
            while ($reg=mysql_fetch_row($resultado))
            {
                $miembros_data[]=array(
                    'Resultado'=>'Exitoso',
                    'Nombres'=>''.$reg['0'].'',
                    'Apellidos'=>''.$reg['1'].'',
                    'Correo'=>''.$reg['3'].'',
                    'Telefono'=>''.$reg['4'].'',

   
    
                   );
            }
 
        }


        $this->dbConexion->disconnect();
        $cad=json_encode ($miembros_data);
        return $cad;



    }

    public function recuperarIdEntrevista()
    {
       $query='select idEntrevista from entrevista 
       where entrevista.Fecha='.$this->entrevista->getHuerta()->getFechaEntrevista().' and
       entrevista.Herta_idHuerta='.$this->DaoHuerta->recuperarIdHuerta().'and
       entrevista.investigador_idInvestigador='.$this->DaoInvestigador->BuscarInvestigador().'';

       $this->dbConexion->conectar();
       $resultado=$this->dbConexion->consulta($query);
  
         $reg=mysql_fetch_row($resultado);
         $this->dbConexion->disconnect();
          return $reg['idEntrevista'];
      

    }

    public function listarCatergoriaPreguntas()
    {

     $query="select * from categoria";     
     $this->dbConexion->conectar();
     $resultado=$this->dbConexion->consulta($query);
     $numeroFilas=$this->dbConexion->numero_de_filas($resultado);
     $listaCategorias_data=Array();

     if($numeroFilas==0)
     {
        $miembros_data[]=array(
            'Resultado'=>'Empty');
     }
     else{

        while ($reg=mysql_fetch_row($resultado))
        {
            $miembros_data[]=array(
            'Resultado'=>'Exito',
            'codigo'=>$reg['0'],
            'descripcion'=>$reg['1']
        );
        
     }
      
     }

     $cad=json_encode ($miembros_data);
     return $cad;

     
    }

    public function listarUnicaEntrevista($idEntrevista)
    {

      $query='select entrevista.Fecha,entrevista.Observaciones,entrevista.Version,
      investigador.Nombres,investigador.Apellidos,investigador.Correo,investigador.Telefono,
      huerta.NombreHuerta,huerta.Ubicacion,huerta.AreaHuerta,huerta.foto        
      from entrevista inner join huerta
      on entrevista.Huerta_idHuerta=huerta.idHuerta 
      inner join investigador on entrevista.investigador_idInvestigador=investigador.idInvestigador  
      where entrevista.idEntrevista='.$idEntrevista.'';



       $this->dbConexion->conectar();
       $resultado=$this->dbConexion->consulta($query);
       $numeroFilas=$this->dbConexion->numero_de_filas($resultado);
       $entrevista_data=array();

       if($numeroFilas==0)
       {
        
        $entrevista_data[]=array(
            'Resultado'=>'Empty');
         
       }
       else
       {

        $reg=mysql_fetch_row($resultado);       
        

        $entrevista_data[]=array(
            'Resultado'=>'Exito',
            'FechaEntrevista'=>''.$reg['0'].'',
            'Observaciones'=>''.$reg['1'].'',
            'Version'=>''.$reg['2'].'',
               'Investigador'=>array(
        
                'Nombres:'=>''.$reg['3'].'',
                'Apellidos'=>''.$reg['4'].'',
                'Correo'=>''.$reg['5'].'',
                'Telefono'=>''.$reg['6'].''
            
               ),
        
        'Huerta'=>array(
        
        
            'NombreHuerta'=>''.$reg['7'].'',
            'ubicacion'=>''.$reg['8'].'',
            'area'=>$reg['9'],
            'foto'=>$reg['10']
        
        )      
        );
        
       }
  

        
         $this->dbConexion->disconnect();
         $cad=json_encode ($entrevista_data);

         return $cad;
         
         
      




    }




    public function AgregarEntrevistado($idEntrevista,$idMiembro)
    {
        $query='insert into entrevistapormiembro (entrevista_idEntrevista,miembro_idMiembro)
        values(
         '.$idEntrevista.',
         '.$idMiembro.'            
       )';
       

       $this->dbConexion->conectar();
       $resultado=$this->dbConexion->consulta($query);
       return $this->VerificarInsercion($idEntrevista,$idMiembro);
       



    }


    public function VerificarInsercion($idEntrevista,$idMiembro)
    {

      $query='select * from EntrevistaPorMiembro
      where entrevista_idEntrevista='.$idEntrevista.' and
      miembro_idMiembro='.$idMiembro.'';

      $this->dbConexion->conectar();
      $resultado=$this->dbConexion->consulta($query);

      $numeroFilas=$this->dbConexion->numero_de_filas($resultado);
      $tex_Insercion=Array();

      if($numeroFilas==0)
      {
          #Fallo en la consulta
          $tex_Insercion[]=array(
            'Resultado'=>'Fallo',
            'CodigoEntrevista'=>$idEntrevista,
            'MiembroEntrevistado'=>$idMiembro

          );
      }

      else
      {
         #Exito en la consula
         $tex_Insercion[]=array(
            'Resultado'=>'Exito',
            'CodigoEntrevista'=>$idEntrevista,
            'MiembroEntrevistado'=>$idMiembro

          );
      }


      $this->dbConexion->disconnect();
      $cad=json_encode ($tex_Insercion);
      return $cad;
      

    
       

    }


    public function listarPreguntaPorCategoria($categoria,$idEntrevista)

    {
            

            $query='select entrevista.idEntrevista,entrevista.Fecha,entrevista.Version,entrevista.Observaciones,huerta.NombreHuerta,pregunta.TituloPregunta,pregunta.TipoPregunta,categoria.idcategoria,categoria.nombreCategoria
            from entrevista inner join huerta on entrevista.Huerta_idHuerta= huerta.idHuerta
            inner join preguntaporentrevista on entrevista.idEntrevista= preguntaporentrevista.Entrevista_idEntrevista
            inner join pregunta on preguntaporentrevista.Pregunta_idPregunta= pregunta.idPregunta
            inner join categoria on pregunta.categoria_idcategoria= categoria.idcategoria
            where categoria.idcategoria='.$categoria.'
            and entrevista.idEntrevista='.$idEntrevista.'';


            $this->dbConexion->conectar();
            $resultado=$this->dbConexion->consulta($query);
            $numeroFilas=$this->dbConexion->numero_de_filas($resultado);
            $pregunta_data=Array();
            $reg=mysql_fetch_row($resultado);    


            if($numeroFilas==0)
            {
                $pregunta_data[]=array(
                'Resultado'=>'Empty'

                );
            }
            else
            {
                $pregunta_data[]=array(

                 'Resultado'=>'Exito',
                 'TituloPregunta'=>$reg['5'],
                 'TipoPregunta'=>$reg['6'],
                 'categoria'=>$reg['7'],
                 'entrevista'=>$reg['0']

                
                );
            }
      

            
        $this->dbConexion->disconnect();
        $cad=json_encode ($pregunta_data);
        return $cad;



    }




public function validacion($codEntrevista)
	
	{
	   $query='select COUNT(miembro.idMiembro)  as Numero
	   from miembro inner join entrevistapormiembro ON entrevistapormiembro.miembro_idMiembro=miembro.idMiembro where entrevistapormiembro.entrevista_idEntrevista='.$codEntrevista.'';
	
	
	  $this->dbConexion->conectar();
      $resultado=$this->dbConexion->consulta($query);
      $numeroFilas=$this->dbConexion->numero_de_filas($resultado);
	  $pregunta_data=Array();
	  $reg=mysql_fetch_row($resultado);
      
	  if($numeroFilas==0)
	  {
	       $pregunta_data[]=array(
                'Resultado'=>'Generado',
				'NumRegistro'=>$reg['0']

                );
	  }
	  
	  else
	  {
	         $pregunta_data[]=array(

                 'Resultado'=>'Generado',
                 'NumRegistro'=>$reg['0']
                 

                
                );       
	  }
	  
	  
	    $this->dbConexion->disconnect();
        $cad=json_encode ($pregunta_data);
        return $cad;
	
	
	}
	
	
	public function AgregarEntrevistadostwo($idEntrevista,$idMiembro)
	{
	    
	    
	     $query='insert into entrevistapormiembro (entrevista_idEntrevista,miembro_idMiembro)
        values(
         '.$idEntrevista.',
         '.$idMiembro.'            
       )';
       

       $this->dbConexion->conectar();
       $resultado=$this->dbConexion->consulta($query);
       //$this->dbConexion->disconnect();
       
	    
	    
	}
	
	
		
	public function asignaPreguntaPorEntrevista($codigoEntrevista)
	{
	   $query='select pregunta.idPregunta,pregunta.TituloPregunta,
	   pregunta.TipoPregunta,categoria.idcategoria,categoria.nombreCategoria from pregunta
	   inner join categoria on pregunta.categoria_idcategoria=categoria.idcategoria';
	   
	  $this->dbConexion->conectar();
      $resultado=$this->dbConexion->consulta($query);
	  
	  while($reg=mysql_fetch_row($resultado))
	  {
	     $this->insertarPreguntaPorEntrevista($codigoEntrevista,$reg['0']);
	  }
	  
	  
	 }
	 
	public function insertarPreguntaPorEntrevista($codigoEntrevista,$codigoPregunta)
	
	{
	   $query='insert into preguntaporentrevista (Entrevista_idEntrevista,Pregunta_idPregunta)
	   values ('.$codigoEntrevista.','.$codigoPregunta.')';
	   
	   $resultado=$this->dbConexion->consulta($query);
	   
	}
	
	
	public function validarOperacionInsercion($codigoEntrevista)
	{
	   $query='select COUNT(idPreguntaPorEntrevista) 
	   as NumeroPreguntas from preguntaporentrevista 
	   where preguntaporentrevista.Entrevista_idEntrevista='.$codigoEntrevista.'';
	   
	   $resultado=$this->dbConexion->consulta($query);
	   $numeroFilas=$this->dbConexion->numero_de_filas($resultado);
	   $reg=mysql_fetch_row($resultado);
	   $operacion_data=Array();
	   
		  
	      if($reg['0']==$this->cuenteNumeroPreguntas())
		  { 
		    $operacion_data[]=array(
			"Respuesta"=>"Exito");
		  }
		  else
		  {
		     $operacion_data[]=array(
			"Respuesta"=>"Falla");
		  }

			
	    $cad=json_encode($operacion_data);
        return $cad;
	   
	   
	   
	   
	   
	}
	
	
	public function cuenteNumeroPreguntas()
	{
	    $query='select count(pregunta.idPregunta) from pregunta';
	    $resultado=$this->dbConexion->consulta($query);
	    $reg=mysql_fetch_row($resultado);
	    
	    return $reg['0'];
	    
	    
	    
	}
	
	public function recuperarVersionEntrevista($codigoHuerta)
	{
	    $query='select COUNT(entrevista.idEntrevista) 
		from entrevista inner join huerta on 
		entrevista.Huerta_idHuerta=huerta.idHuerta where entrevista.Huerta_idHuerta='.$codigoHuerta.'';
	    $this->dbConexion->conectar();
		$resultado=$this->dbConexion->consulta($query);
		$version=Array();		
		$reg=mysql_fetch_row($resultado);
		
		$version[]=array(
		
		   'Resultado'=>'Exito',
		   'Version'=>$reg['0']
		);
		
		$cad=json_encode($version);
        return $cad;
	   
		
		
		
	}
	
	
	
	








     





}





?>