-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Tiempo de generación: 07-08-2018 a las 17:42:53
-- Versión del servidor: 10.1.31-MariaDB
-- Versión de PHP: 7.0.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `id6199571_rhmdatabase`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categoria`
--

CREATE TABLE `categoria` (
  `idcategoria` int(11) NOT NULL,
  `nombreCategoria` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `categoria`
--

INSERT INTO `categoria` (`idcategoria`, `nombreCategoria`) VALUES
(1, 'Descripcion y Caracterizacion de Huerta'),
(2, 'Relaciones con la comunidad,Colectivos u organizaciones'),
(3, 'Practicas de Siembra'),
(4, 'Practicas para el mantenimiento de la huerta'),
(5, 'Equipo Colaborador');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `entrevista`
--

CREATE TABLE `entrevista` (
  `idEntrevista` int(11) NOT NULL,
  `Fecha` date NOT NULL,
  `Observaciones` varchar(500) NOT NULL,
  `Version` varchar(45) NOT NULL,
  `Huerta_idHuerta` int(11) NOT NULL,
  `investigador_idInvestigador` int(11) NOT NULL,
  `estado_entrevista` enum('Progreso','Terminada','','') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `entrevista`
--

INSERT INTO `entrevista` (`idEntrevista`, `Fecha`, `Observaciones`, `Version`, `Huerta_idHuerta`, `investigador_idInvestigador`, `estado_entrevista`) VALUES
(37, '2018-08-06', 'Se genera la entrevista.', '1', 23, 2, 'Progreso'),
(38, '2018-08-06', 'Observacion 1', '1', 27, 3, 'Progreso'),
(39, '2018-08-06', 'se genera observacion', '1', 25, 1, 'Progreso');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `entrevistapormiembro`
--

CREATE TABLE `entrevistapormiembro` (
  `idEntrevistaPorMiembro` int(11) NOT NULL,
  `entrevista_idEntrevista` int(11) NOT NULL,
  `miembro_idMiembro` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `entrevistapormiembro`
--

INSERT INTO `entrevistapormiembro` (`idEntrevistaPorMiembro`, `entrevista_idEntrevista`, `miembro_idMiembro`) VALUES
(1, 37, 54),
(2, 37, 55),
(3, 38, 60),
(4, 39, 58);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `huerta`
--

CREATE TABLE `huerta` (
  `idHuerta` int(11) NOT NULL,
  `NombreHuerta` varchar(500) NOT NULL,
  `Ubicacion` varchar(500) NOT NULL,
  `AreaHuerta` float NOT NULL,
  `foto` varchar(45) NOT NULL,
  `Coordenada` point NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `huerta`
--

INSERT INTO `huerta` (`idHuerta`, `NombreHuerta`, `Ubicacion`, `AreaHuerta`, `foto`, `Coordenada`) VALUES
(19, 'El Socorro', 'Sur Oriente', 22, 'Picture.jpg', ''),
(20, 'La Nubia', 'Sur Oriente', 23, 'picture.jpg', ''),
(21, 'La Rosa', 'Sur Oriente', 23, 'picture.jpg', ''),
(22, 'El chorro', 'Nor Oriente', 33, 'Picture.jpg', ''),
(23, 'Alcazares', 'calle 55', 22, 'picture.jps', ''),
(24, 'La Rochela', 'Sur Occidente', 23, 'picture.jpg', ''),
(25, 'Colonia', 'sur oriente', 23, 'la foto.jpg', ''),
(26, 'Filo de Hambre', 'Coordenada', 23, 'Foto.jpg', ''),
(27, 'El Perpetuo', '6.235420599999999,-75.6163828', 22, 'Picture.jpg', ''),
(28, 'La Celia', '6.2352621,-75.6163062', 22, 'Foto', ''),
(29, 'La Perdiz', '6.2353017,-75.6164249', 23, '', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `huertapormiembro`
--

CREATE TABLE `huertapormiembro` (
  `idHuertaPorMiembros` int(11) NOT NULL,
  `Huerta_idHuerta` int(11) NOT NULL,
  `Miembro_idMiembro` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `huertapormiembro`
--

INSERT INTO `huertapormiembro` (`idHuertaPorMiembros`, `Huerta_idHuerta`, `Miembro_idMiembro`) VALUES
(27, 19, 48),
(28, 21, 49),
(29, 21, 50),
(30, 21, 51),
(31, 22, 52),
(32, 22, 53),
(33, 23, 54),
(34, 23, 55),
(35, 19, 56),
(36, 24, 57),
(37, 25, 58),
(38, 26, 59),
(39, 27, 60);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `investigador`
--

CREATE TABLE `investigador` (
  `idInvestigador` int(11) NOT NULL,
  `Nombres` varchar(45) NOT NULL,
  `Apellidos` varchar(45) NOT NULL,
  `Correo` varchar(45) NOT NULL,
  `Telefono` varchar(45) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `investigador`
--

INSERT INTO `investigador` (`idInvestigador`, `Nombres`, `Apellidos`, `Correo`, `Telefono`, `username`, `password`) VALUES
(1, 'Carlos', 'Benavidez', 'car@gmail.com', '49495', 'admin', 'admin'),
(2, 'Paula  Andrea', 'Restrepo', 'paula.restrepo@udea.edu.co', '', 'paula.restrepo', 'Entrevistadora'),
(3, 'Kelly', 'Mano', 'kelly@gmail.com', '34565', 'kelly.mano', 'Entrevistadora1');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `miembro`
--

CREATE TABLE `miembro` (
  `idMiembro` int(11) NOT NULL,
  `Nombres` varchar(50) NOT NULL,
  `Apellidos` varchar(45) NOT NULL,
  `Genero` enum('M','F') NOT NULL,
  `Correo` varchar(45) DEFAULT NULL,
  `Telefono` varchar(45) DEFAULT NULL,
  `Rol` enum('Colaborador','Responsable','','') NOT NULL DEFAULT 'Colaborador'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `miembro`
--

INSERT INTO `miembro` (`idMiembro`, `Nombres`, `Apellidos`, `Genero`, `Correo`, `Telefono`, `Rol`) VALUES
(1, 'Mario', 'Gonzales', 'M', 'pacho@gmail.com', '39494', ''),
(2, 'Mario', 'Moreno', 'M', 'ma@gmail.com', '1344', ''),
(18, 'Duvan', 'Gomez', 'M', 'duv@gmail.com', '49595', ''),
(19, 'Duvan', 'Gomez', 'M', 'duv@gmail.com', '49595', ''),
(20, 'Gabriel', 'Rodriguez', 'M', 'gabo@gmail.com', '9559', ''),
(21, 'Rodrigo', 'Pelaez', 'M', 'rode@gmail.com', '3855', ''),
(22, 'Carlos', 'Gonzales', 'M', 'caliche@gmail.com', '49595', ''),
(23, 'Cristian', 'Espinosa', 'M', 'crist@gmail.com', '9494', ''),
(24, 'Ismael', 'Gomez', 'M', 'gome@hotmail.com', '455', ''),
(25, 'Ismael', 'Gomez', 'M', 'gome@hotmail.com', '455', ''),
(26, 'Hugo', 'Perseo', 'M', 'per@hotmail.com', '455', ''),
(27, 'hugit', 'Gaus', 'M', 'cake@hotmail.com', '4555', ''),
(28, 'Carlos', 'Gomez', 'M', 'carlos@gmail.com', '355', ''),
(29, 'Miguel', 'Antonio', 'M', 'carlos@hotmail.com', '23455', ''),
(30, 'Yerri', 'Mina', 'M', 'ye@gmail.com', '3345', ''),
(31, 'Carmela', 'Valencia', 'F', 'car@gmail.com', '38384', ''),
(32, 'Irwin', 'Mathew', 'M', 'mat@ggmail.com', '34566', ''),
(33, 'Judit', 'Castro', 'F', 'jud@hotmail.com', '2334', ''),
(34, 'German', 'Gaviria', 'M', 'gemanco@gmail.com', '234545', ''),
(35, 'Yuri', 'Gaga', 'F', 'ca@gmail.com', '12244', ''),
(36, '', '', '', '', '', ''),
(37, 'Fernando', 'Chivo', 'M', 'fercho@gmail.com', '123445', ''),
(38, 'Davinso', 'Sanchez', 'M', 'carlos@hotmail.com', '34556', ''),
(39, 'Cornelio ', 'Gomez', 'M', 'cor@gmail.com', '123445', ''),
(40, 'Tiomonel', 'Perlaza', 'M', 'ti@gmail.com', '234566', ''),
(41, 'Luis Alberto', 'Benavidez España', 'M', 'lusisi@gmail.com', '33455', ''),
(42, 'Julio', 'Verne', 'M', 'juli@gmail.com', '23455', ''),
(43, 'David', 'Gonzales', 'M', 'davicho@gmail.com', '2233444', ''),
(44, 'Rodrigo', 'Perlaza', 'M', 'rodir@gmail.com', '234455', ''),
(45, 'Ernesto', 'Romero', 'M', 'ern@gmail.com', '234556', ''),
(46, 'Danyer', 'Gomez', 'M', 'goe@gmail.com', '23455', ''),
(47, 'Susana ', 'Mora', 'F', 'sus@gmail.com', '2244', ''),
(48, 'Jorge ', 'Robledo', 'M', 'email@gmail.com', '3464', ''),
(49, 'Jorge', 'Isaac', 'M', 'isac@gmai.com', '34455', ''),
(50, 'Guillermo', 'Leon', 'M', 'guiell@gmail.com', '3455', ''),
(51, 'Margarita', 'Gomez', 'F', 'margara@gmail.com', '23455', ''),
(52, 'Camilo', 'Sierra', 'M', 'prueba@gmail.com', '233454', ''),
(53, 'Jualian', 'Medellin', 'M', 'eljuli@gmail.com', '22345', ''),
(54, 'Javier', 'Burgos', 'M', 'javierburgos1414@gmail.com', '3117616062', ''),
(55, 'Cristina', 'Sandoval', 'F', 'cris@gmail.com', '33456', ''),
(56, 'Rafael', 'Pardo', 'M', 'rafa@gmail.com', '455554', 'Responsable'),
(57, 'Luis', 'Benavidez', 'M', 'luis@gmail.com', '93945', 'Responsable'),
(58, 'luis', 'gonzale', 'M', 'luis@gmail.com', '27474', ''),
(59, 'Daniel', 'Benavidez', 'M', 'Dani@gmail.com', '394945', 'Responsable'),
(60, 'Miller ', 'Gomez', 'M', 'miller@gmail.com', '39545', 'Colaborador');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `opcionrespuesta`
--

CREATE TABLE `opcionrespuesta` (
  `idOpcionRespuesta` int(11) NOT NULL,
  `Pregunta_idPregunta` int(11) NOT NULL,
  `DescripcionOpcionRespuesta` varchar(500) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `opcionrespuesta`
--

INSERT INTO `opcionrespuesta` (`idOpcionRespuesta`, `Pregunta_idPregunta`, `DescripcionOpcionRespuesta`) VALUES
(1, 3, 'Ingrese su respuesta.'),
(2, 4, 'Ingrese Tiempo en en meses'),
(3, 5, 'Ingrese Respuesta'),
(5, 6, 'Antejardín'),
(6, 6, 'Balcón'),
(7, 6, 'Terraza'),
(8, 6, 'Pared'),
(9, 6, 'Espacio Público'),
(10, 6, 'Interiores'),
(11, 6, 'Otro(s)'),
(12, 7, 'Casa o apartamento'),
(13, 7, 'Unidades residenciales'),
(14, 7, 'Escolar'),
(15, 7, 'Empresarial '),
(16, 7, 'Biblioteca'),
(17, 7, 'Casa de cultura'),
(18, 7, 'Otro(s)'),
(19, 8, 'Familiar '),
(20, 8, 'Comunitaria'),
(21, 8, 'Pública '),
(22, 8, 'Privada '),
(23, 9, 'Nivelaron'),
(24, 9, 'Rellenaron'),
(25, 9, 'Cambiaron la tierra'),
(26, 9, 'Abonaron la tierra'),
(27, 9, 'Construyeron cerramiento'),
(28, 9, 'Ya contaba con cerramiento '),
(29, 9, 'Otro(s)'),
(30, 10, 'Ingrese Respuesta'),
(31, 11, 'Pala'),
(32, 11, 'Pica'),
(33, 11, 'Rastrillo'),
(34, 11, 'Manguera'),
(35, 11, 'Regadera'),
(36, 11, 'Tijeras'),
(37, 11, 'Barra'),
(38, 11, 'Palín'),
(39, 11, 'Machete'),
(40, 11, 'Azadón'),
(41, 11, 'Cuchillo '),
(42, 11, 'Otro(s)'),
(43, 12, 'Explicación: ¿Cómo era el espacio antes y después de la intervención?\r\n'),
(44, 13, 'Nota:pedir nombres propios. Esto con la intención de mapear las interacciones de la gente en diferentes huertas.\r\n'),
(45, 14, 'Nota:pedir nombres propios. Esto con la intención de mapear las interacciones de la gente en diferentes huertas.\r\n'),
(46, 15, 'Ingrese Respuesta'),
(47, 16, 'Ingrese Respuesta'),
(48, 17, 'Sí'),
(49, 17, 'No'),
(50, 18, '¿Han asistido a reuniones o eventos de la RHM?'),
(51, 18, '¿Participan en las redes sociales de la RHM?'),
(52, 18, '¿Son parte del grupo base de la RHM? '),
(53, 18, 'Otro(s)'),
(54, 20, 'Ningún aporte'),
(55, 20, 'Escaso'),
(56, 20, 'Algún aporte'),
(57, 20, 'Aporte significativo'),
(58, 20, 'Aporte indispensable'),
(59, 21, 'Ingrese Respuesta'),
(60, 22, 'Admiración'),
(61, 22, 'Apoyo'),
(62, 22, 'Rechazo'),
(63, 22, 'Indiferencia/desconocimiento'),
(64, 22, 'Otro(s)'),
(65, 23, 'Admiración '),
(66, 23, 'Apoyo'),
(67, 23, 'Rechazo'),
(68, 23, 'Indiferencia/desconocimiento'),
(69, 23, 'Otro(s)'),
(70, 24, 'Sí'),
(71, 24, 'No'),
(72, 25, 'Sí'),
(73, 25, 'No'),
(74, 26, 'Cine '),
(75, 26, 'Intervención artística'),
(76, 26, 'Seminarios'),
(77, 26, 'Tertulias'),
(78, 26, 'Talleres'),
(79, 26, 'Visitas guiadas'),
(80, 26, 'Otro(s)'),
(81, 27, 'Sí'),
(82, 27, 'No'),
(83, 28, 'Ingrese su Respuesta.'),
(84, 29, ' Sí '),
(86, 29, 'No'),
(87, 30, 'Siembra de plántulas'),
(88, 30, 'Siembra de semillas'),
(89, 30, 'Esquejes'),
(90, 30, 'Piecito'),
(91, 30, 'Otro(s)'),
(92, 31, 'Sí '),
(93, 31, 'No'),
(94, 32, 'Sobre la tierra'),
(95, 32, 'En recipientes de diversos tipos'),
(96, 32, 'Aromáticas y Medicinales'),
(97, 32, 'Hortalizas'),
(98, 32, 'Ornamentales'),
(99, 32, 'Frutales'),
(100, 33, 'Atractores de abejas y mariposas y otros insectos polinizadores '),
(101, 33, 'Otro(s)'),
(102, 34, 'Ingrese su respuesta'),
(103, 35, 'Ingrese su Repuesta'),
(104, 36, 'Ingreso su Respuesta'),
(107, 37, 'Guardianes de semillas'),
(108, 37, 'Mis propias semillas'),
(109, 37, 'Intercambios con otros huerteros'),
(110, 37, 'Tiendas agrícolas o establecimientos comerciales'),
(111, 37, 'Alcaldía u otros organismos estatales'),
(112, 37, 'Otro(s)'),
(113, 38, 'Ingrese Respuesta'),
(114, 39, 'Sí'),
(115, 39, 'No'),
(116, 40, 'Ingrese su Respuesta'),
(117, 41, 'Las usan todas en la siembra '),
(118, 41, 'Las intercambian'),
(119, 41, 'Las venden'),
(120, 41, 'Las desechan'),
(121, 41, 'Se las comen'),
(122, 41, 'Las utilizan en la fabricación de artesanías'),
(123, 41, 'Otro(s)'),
(124, 42, 'Autoconsumo'),
(125, 42, 'Compartir con los vecinos'),
(126, 42, 'Venta'),
(127, 42, 'Intercambio'),
(128, 42, 'No se recolectan'),
(129, 42, 'Otro(s)'),
(130, 43, 'Ingrese su respuesta'),
(131, 44, 'Ingrese su respuesta'),
(132, 45, 'Sí'),
(133, 45, 'No'),
(134, 46, 'Compost'),
(135, 46, 'Lombricultivo'),
(136, 46, 'Paca'),
(137, 46, 'Orgánicos comerciales'),
(138, 46, 'Agroquímicos'),
(139, 46, 'Otro(s)'),
(140, 47, 'Ingrese su Respuesta'),
(141, 48, 'Ingrese su Respuesta'),
(142, 49, 'Ancho'),
(143, 49, 'Largo'),
(144, 49, 'Alto'),
(145, 50, 'Rechazo'),
(146, 50, 'Indiferencia'),
(147, 50, 'Colaboran llevando residuos'),
(148, 50, 'Colaboran en la construcción de las pacas'),
(149, 51, 'Taller con Guillermo Silva'),
(150, 51, 'Internet'),
(151, 51, 'Otros Huerteros'),
(152, 51, 'Otro(s)'),
(153, 52, 'Producción propia'),
(154, 52, 'Proveedores comerciales'),
(155, 52, 'Intercambio con otras huertas.'),
(156, 53, 'Sí'),
(157, 53, 'No'),
(158, 54, 'Biopreparados'),
(159, 54, 'Interacción entre plantas - Alelopatía'),
(160, 54, 'Industriales'),
(161, 54, 'Nada'),
(162, 54, 'Otro(s)'),
(163, 55, 'Regadera'),
(164, 55, 'Manguera'),
(165, 55, 'Control de riego mecánico'),
(166, 55, 'Control de riego automático'),
(167, 55, 'Botellas enterradas'),
(168, 55, 'Otro(s)'),
(169, 56, 'Semillas '),
(170, 56, 'Abono'),
(171, 56, 'Voluntarios'),
(172, 56, 'Mantenimiento'),
(173, 56, 'Asesoría'),
(174, 56, 'Otro(s)'),
(175, 57, 'Ingrese su respuesta.'),
(176, 58, 'Ingrese su respuesta.'),
(177, 59, '12'),
(178, 59, '13-14'),
(179, 59, '19-24'),
(180, 59, '25-35'),
(181, 59, '36-50'),
(182, 59, '51-62'),
(183, 59, '+63'),
(184, 60, 'Ingrese su Respuesta'),
(185, 61, 'Ingrese su Respuesta'),
(186, 62, 'Ingrese su Respuesta'),
(187, 63, 'Ingrese su Respuesta'),
(188, 64, 'Ingrese su Respuesta'),
(189, 65, 'Ingrese su Respusta'),
(190, 19, 'Ingrese su Respuesta');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pregunta`
--

CREATE TABLE `pregunta` (
  `idPregunta` int(11) NOT NULL,
  `TituloPregunta` varchar(500) NOT NULL,
  `TipoPregunta` enum('Seleccion','Abierta') NOT NULL,
  `categoria_idcategoria` int(11) NOT NULL,
  `OpcionRespuesta` varchar(45) DEFAULT NULL,
  `Pista` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `pregunta`
--

INSERT INTO `pregunta` (`idPregunta`, `TituloPregunta`, `TipoPregunta`, `categoria_idcategoria`, `OpcionRespuesta`, `Pista`) VALUES
(3, 'Que razon tuvieron para ponerle ese nombre', 'Abierta', 1, NULL, ''),
(4, 'Antigüedad: (hace cuánto existe la huerta) (en meses)', 'Abierta', 1, NULL, ''),
(5, '¿Tiene su huerta alguna forma particular? ¿Cuál?', 'Abierta', 1, NULL, ''),
(6, 'Lugar de la Huerta', 'Seleccion', 1, 'Multiple', ''),
(7, 'Tipo de huerta por el espacio que ocupa', 'Seleccion', 1, 'Unica', ''),
(8, 'Tipo de huerta por sus participantes', 'Seleccion', 1, 'Unica', ''),
(9, '¿Cómo han adaptado el espacio para la construcción de la huerta?', 'Seleccion', 1, '	\r\nMultiple', ''),
(10, '¿Qué tipo de cerramiento tiene?', 'Abierta', 1, NULL, ''),
(11, '¿Qué herramientas tienen en la huerta?', 'Seleccion', 1, 'Multiple', ''),
(12, '¿Cómo ha transformado la presencia de la huerta el espacio donde se encuentra? \r\n', 'Abierta', 1, NULL, ''),
(13, '¿Qué personas, colectivos, organizaciones o instituciones se han relacionado con el proceso de la huerta?', 'Abierta', 2, NULL, ''),
(14, '¿De esas personas, colectivos, organizaciones e instituciones mencionadas, cuáles han sido fundamentales en el proceso de consolidación y mantenimiento de la huerta?\r\n', 'Abierta', 2, NULL, ''),
(15, '¿Cómo se conectaron con estas personas, colectivos, organizaciones o instituciones que han sido fundamentales en el proceso?\r\n', 'Abierta', 2, NULL, ''),
(16, '¿Por qué son fundamentales?', 'Abierta', 2, NULL, ''),
(17, '¿Han oído hablar o han interactuado con la RHM? \r\n', 'Seleccion', 2, 'Unica', ''),
(18, '¿Qué relación han tenido con la RHM?', 'Seleccion', 2, 'Multiple', ''),
(19, '¿Qué le ha aportado la RHM a esta huerta? \r\n', 'Abierta', 2, '¿En Conocimiento?,¿En relacionamiento con per', ''),
(20, 'Evalúe el aporte de la RHM a esta huerta', 'Seleccion', 2, 'Unica', ''),
(21, '¿Qué otras personas, colectivos, organizaciones o instituciones serían útiles para el trabajo en esta huerta?', 'Abierta', 2, NULL, ''),
(22, 'En caso de ser familiar, ¿cómo es la relación de la familia con la huerta?', 'Seleccion', 2, 'Unica', ''),
(23, '¿Cómo es la relación de los vecinos con la huerta?', 'Seleccion', 2, NULL, ''),
(24, 'La aparición de esta huerta ¿ha propiciado otras iniciativas similares en la zona? ', 'Seleccion', 2, 'Unica', ''),
(25, '¿Se han desarrollado procesos de enseñanza/aprendizaje a partir de la huerta? \r\n', 'Seleccion', 2, 'Unica', ''),
(26, '¿Realizan actividades diferentes a la siembra en la huerta?', 'Seleccion', 2, 'Multiple', ''),
(27, '¿Interactúan con otras huertas vecinas?', 'Seleccion', 2, 'Unica', ''),
(28, '¿Con cuáles huertas interactúan?', 'Abierta', 2, NULL, ''),
(29, '¿Le gustaría compartir sus saberes con otros? ', 'Seleccion', 2, 'Unica', ''),
(30, '¿Cómo es la reproducción de las plantas en su huerta?', 'Seleccion', 3, 'Unica', ''),
(31, '¿Tienen Semillero/germinadero?', 'Seleccion', 3, 'Unica', ''),
(32, '¿Cuál es su forma de siembra?', 'Seleccion', 3, 'Multiple', ''),
(33, '¿Qué se siembra en la huerta actualmente?', 'Seleccion', 3, 'Multiple', ''),
(34, '¿Qué razones han tenido para sembrar estas plantas?', 'Abierta', 3, NULL, ''),
(35, '¿Qué han sembrado sin éxito? \r\n', 'Abierta', 3, 'Indicar un listado de plantas', ''),
(36, 'Qué motivó el fracaso de esas siembras?\r\n', 'Abierta', 3, NULL, ''),
(37, '¿Cómo obtienen las semillas que se siembran? ', 'Seleccion', 3, 'Unica', ''),
(38, '¿Qué razones tiene para obtener de ese modo sus semillas?', 'Abierta', 3, NULL, ''),
(39, '¿Conservan las semillas de su huerta? ', 'Seleccion', 3, 'Unica', ''),
(40, '¿Cómo conservan las semillas?', 'Abierta', 3, NULL, ''),
(41, '¿Qué hacen con las semillas que produce la huerta?', 'Seleccion', 3, 'Multiple', ''),
(42, '¿Qué hacen con los productos de la huerta?', 'Seleccion', 3, 'Unica', ''),
(43, '¿Cómo han obtenido los conocimientos que aplican en su huerta? ', 'Abierta', 3, NULL, ''),
(44, 'Cuando tienen dudas ¿a qué fuentes recurren?', 'Abierta', 3, NULL, ''),
(45, '¿Abonan la huerta?', 'Seleccion', 4, 'Unica', ''),
(46, '¿Qué abono utilizan en la huerta?', 'Seleccion', 4, 'Multiple', ''),
(47, '¿Desde hace cuántos meses están haciendo pacas?', 'Abierta', 4, NULL, ''),
(48, '¿Cuántas han completado?', 'Abierta', 4, NULL, ''),
(49, '¿Cuáles son las dimensiones promedio de esas pacas en metros cúbicos? ', 'Abierta', 4, NULL, ''),
(50, 'Relación de los vecinos con las pacas', 'Seleccion', 4, 'Multiple', ''),
(51, '¿Cómo aprendieron a hacer pacas?', 'Seleccion', 4, 'Multiple', ''),
(52, '¿De dónde procede el abono?', 'Seleccion', 4, 'Unica', ''),
(53, '¿Usan algún producto para controlar plagas?', 'Seleccion', 4, 'Unica', ''),
(54, '¿Qué productos o sistemas utilizan en la huerta para el control de plagas?', 'Seleccion', 4, 'Multiple', ''),
(55, 'Modo de riego', 'Seleccion', 4, 'Multiple', ''),
(56, '¿Qué necesidades tiene la huerta en este momento?', 'Seleccion', 4, 'Multiple', ''),
(57, '¿Quiénes son los principales responsables del cuidado de la huerta?', 'Abierta', 5, NULL, ''),
(58, '¿Cómo se conformó el equipo?', 'Abierta', 5, NULL, ''),
(59, '¿Qué edades tienen los principales responsables del cuidado?', 'Seleccion', 5, 'Multiple', ''),
(60, '¿Cuál es la ocupación o profesión de los principales responsables?', 'Abierta', 5, NULL, ''),
(61, '¿Cómo se comunica el equipo de trabajo?', 'Abierta', 5, NULL, ''),
(62, '¿Cuántos hombres y cuántas mujeres trabajan directamente en la huerta?', 'Abierta', 5, NULL, 'Ingrese su Respuesta en el siguiente formato: M:Numero,F:Numero'),
(63, '\r\n¿Ha habido tensiones o conflictos entre las personas que trabajan en la huerta para organizar su trabajo?¿de qué tipo?\r\n', 'Abierta', 5, NULL, ''),
(64, '¿Cuántas horas a la semana dedican al cuidado de la huerta?', 'Abierta', 5, NULL, 'Ingrese el Numero de Horas ej:48'),
(65, '¿Qué razones tuvieron para hacer esta huerta y cómo han cambiado?', 'Abierta', 5, NULL, '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `preguntaporentrevista`
--

CREATE TABLE `preguntaporentrevista` (
  `idPreguntaPorEntrevista` int(11) NOT NULL,
  `Entrevista_idEntrevista` int(11) NOT NULL,
  `Pregunta_idPregunta` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `preguntaporentrevista`
--

INSERT INTO `preguntaporentrevista` (`idPreguntaPorEntrevista`, `Entrevista_idEntrevista`, `Pregunta_idPregunta`) VALUES
(1, 37, 3),
(2, 37, 4),
(3, 37, 5),
(4, 37, 6),
(5, 37, 7),
(6, 37, 8),
(7, 37, 9),
(8, 37, 10),
(9, 37, 11),
(10, 37, 12),
(11, 37, 13),
(12, 37, 14),
(13, 37, 15),
(14, 37, 16),
(15, 37, 17),
(16, 37, 18),
(17, 37, 19),
(18, 37, 20),
(19, 37, 21),
(20, 37, 22),
(21, 37, 23),
(22, 37, 24),
(23, 37, 25),
(24, 37, 26),
(25, 37, 27),
(26, 37, 28),
(27, 37, 29),
(28, 37, 30),
(29, 37, 31),
(30, 37, 32),
(31, 37, 33),
(32, 37, 34),
(33, 37, 35),
(34, 37, 36),
(35, 37, 37),
(36, 37, 38),
(37, 37, 39),
(38, 37, 40),
(39, 37, 41),
(40, 37, 42),
(41, 37, 43),
(42, 37, 44),
(43, 37, 45),
(44, 37, 46),
(45, 37, 47),
(46, 37, 48),
(47, 37, 49),
(48, 37, 50),
(49, 37, 51),
(50, 37, 52),
(51, 37, 53),
(52, 37, 54),
(53, 37, 55),
(54, 37, 56),
(55, 37, 57),
(56, 37, 58),
(57, 37, 59),
(58, 37, 60),
(59, 37, 61),
(60, 37, 62),
(61, 37, 63),
(62, 37, 64),
(63, 37, 65),
(64, 38, 3),
(65, 38, 4),
(66, 38, 5),
(67, 38, 6),
(68, 38, 7),
(69, 38, 8),
(70, 38, 9),
(71, 38, 10),
(72, 38, 11),
(73, 38, 12),
(74, 38, 13),
(75, 38, 14),
(76, 38, 15),
(77, 38, 16),
(78, 38, 17),
(79, 38, 18),
(80, 38, 19),
(81, 38, 20),
(82, 38, 21),
(83, 38, 22),
(84, 38, 23),
(85, 38, 24),
(86, 38, 25),
(87, 38, 26),
(88, 38, 27),
(89, 38, 28),
(90, 38, 29),
(91, 38, 30),
(92, 38, 31),
(93, 38, 32),
(94, 38, 33),
(95, 38, 34),
(96, 38, 35),
(97, 38, 36),
(98, 38, 37),
(99, 38, 38),
(100, 38, 39),
(101, 38, 40),
(102, 38, 41),
(103, 38, 42),
(104, 38, 43),
(105, 38, 44),
(106, 38, 45),
(107, 38, 46),
(108, 38, 47),
(109, 38, 48),
(110, 38, 49),
(111, 38, 50),
(112, 38, 51),
(113, 38, 52),
(114, 38, 53),
(115, 38, 54),
(116, 38, 55),
(117, 38, 56),
(118, 38, 57),
(119, 38, 58),
(120, 38, 59),
(121, 38, 60),
(122, 38, 61),
(123, 38, 62),
(124, 38, 63),
(125, 38, 64),
(126, 38, 65),
(127, 39, 3),
(128, 39, 4),
(129, 39, 5),
(130, 39, 6),
(131, 39, 7),
(132, 39, 8),
(133, 39, 9),
(134, 39, 10),
(135, 39, 11),
(136, 39, 12),
(137, 39, 13),
(138, 39, 14),
(139, 39, 15),
(140, 39, 16),
(141, 39, 17),
(142, 39, 18),
(143, 39, 19),
(144, 39, 20),
(145, 39, 21),
(146, 39, 22),
(147, 39, 23),
(148, 39, 24),
(149, 39, 25),
(150, 39, 26),
(151, 39, 27),
(152, 39, 28),
(153, 39, 29),
(154, 39, 30),
(155, 39, 31),
(156, 39, 32),
(157, 39, 33),
(158, 39, 34),
(159, 39, 35),
(160, 39, 36),
(161, 39, 37),
(162, 39, 38),
(163, 39, 39),
(164, 39, 40),
(165, 39, 41),
(166, 39, 42),
(167, 39, 43),
(168, 39, 44),
(169, 39, 45),
(170, 39, 46),
(171, 39, 47),
(172, 39, 48),
(173, 39, 49),
(174, 39, 50),
(175, 39, 51),
(176, 39, 52),
(177, 39, 53),
(178, 39, 54),
(179, 39, 55),
(180, 39, 56),
(181, 39, 57),
(182, 39, 58),
(183, 39, 59),
(184, 39, 60),
(185, 39, 61),
(186, 39, 62),
(187, 39, 63),
(188, 39, 64),
(189, 39, 65);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `respuestaregistrada`
--

CREATE TABLE `respuestaregistrada` (
  `idRespuestaRegistrada` int(11) NOT NULL,
  `OpcionRespuesta_idOpcionRespuesta` int(11) NOT NULL,
  `pregunta` int(11) DEFAULT NULL,
  `contenido` varchar(500) NOT NULL,
  `preguntaporentrevista_idPreguntaPorEntrevista` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `respuestaregistrada`
--

INSERT INTO `respuestaregistrada` (`idRespuestaRegistrada`, `OpcionRespuesta_idOpcionRespuesta`, `pregunta`, `contenido`, `preguntaporentrevista_idPreguntaPorEntrevista`) VALUES
(1, 18, 7, 'Casa Campestres', 5),
(2, 19, 8, '', 6),
(3, 179, 59, '', 57),
(4, 180, 59, '', 57),
(5, 181, 59, '', 57),
(6, 177, 59, '', 57),
(7, 58, 58, 'En una reunion del barrio', 56),
(8, 60, 60, 'Profesores,Estudiantes', 58),
(9, 42, 11, '', 9),
(10, 31, 11, '', 9),
(11, 32, 11, '', 9),
(14, 57, 57, 'Julian y Camilo Sanchez', 55),
(15, 62, 62, 'Hombres:23, Mujeres:19', 60),
(16, 23, 9, '', 7),
(19, 26, 9, '', 7),
(20, 64, 64, '18', 62),
(21, 33, 11, '', 9),
(22, 119, 41, '', 165),
(23, 120, 41, '', 165),
(24, 121, 41, '', 165),
(25, 18, 7, 'Universidades', 68),
(26, 3, 3, 'Autoria del Lider Comunitarios', 127),
(27, 42, 11, '', 135);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE `usuario` (
  `id` int(11) NOT NULL,
  `username` varchar(30) NOT NULL,
  `password` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `categoria`
--
ALTER TABLE `categoria`
  ADD PRIMARY KEY (`idcategoria`);

--
-- Indices de la tabla `entrevista`
--
ALTER TABLE `entrevista`
  ADD PRIMARY KEY (`idEntrevista`),
  ADD KEY `fk_Entrevista_Huerta1_idx` (`Huerta_idHuerta`),
  ADD KEY `fk_entrevista_investigador1_idx` (`investigador_idInvestigador`);

--
-- Indices de la tabla `entrevistapormiembro`
--
ALTER TABLE `entrevistapormiembro`
  ADD PRIMARY KEY (`idEntrevistaPorMiembro`,`entrevista_idEntrevista`,`miembro_idMiembro`),
  ADD KEY `fk_EntrevistaPorMiembro_entrevista1_idx` (`entrevista_idEntrevista`),
  ADD KEY `fk_EntrevistaPorMiembro_miembro1_idx` (`miembro_idMiembro`);

--
-- Indices de la tabla `huerta`
--
ALTER TABLE `huerta`
  ADD PRIMARY KEY (`idHuerta`);

--
-- Indices de la tabla `huertapormiembro`
--
ALTER TABLE `huertapormiembro`
  ADD PRIMARY KEY (`idHuertaPorMiembros`,`Huerta_idHuerta`,`Miembro_idMiembro`),
  ADD KEY `fk_HuertaPorMiembros_Huerta1_idx` (`Huerta_idHuerta`),
  ADD KEY `fk_HuertaPorMiembros_Miembro1_idx` (`Miembro_idMiembro`);

--
-- Indices de la tabla `investigador`
--
ALTER TABLE `investigador`
  ADD PRIMARY KEY (`idInvestigador`);

--
-- Indices de la tabla `miembro`
--
ALTER TABLE `miembro`
  ADD PRIMARY KEY (`idMiembro`);

--
-- Indices de la tabla `opcionrespuesta`
--
ALTER TABLE `opcionrespuesta`
  ADD PRIMARY KEY (`idOpcionRespuesta`,`Pregunta_idPregunta`),
  ADD KEY `fk_OpcionRespuesta_Pregunta1_idx` (`Pregunta_idPregunta`);

--
-- Indices de la tabla `pregunta`
--
ALTER TABLE `pregunta`
  ADD PRIMARY KEY (`idPregunta`,`categoria_idcategoria`),
  ADD KEY `fk_pregunta_categoria1_idx` (`categoria_idcategoria`);

--
-- Indices de la tabla `preguntaporentrevista`
--
ALTER TABLE `preguntaporentrevista`
  ADD PRIMARY KEY (`idPreguntaPorEntrevista`),
  ADD KEY `fk_PreguntaPorEntrevista_Entrevista1_idx` (`Entrevista_idEntrevista`),
  ADD KEY `fk_PreguntaPorEntrevista_Pregunta1_idx` (`Pregunta_idPregunta`);

--
-- Indices de la tabla `respuestaregistrada`
--
ALTER TABLE `respuestaregistrada`
  ADD PRIMARY KEY (`idRespuestaRegistrada`),
  ADD KEY `fk_RespuestaRegistrada_OpcionRespuesta1_idx` (`OpcionRespuesta_idOpcionRespuesta`),
  ADD KEY `pregunta_idx` (`pregunta`);

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `categoria`
--
ALTER TABLE `categoria`
  MODIFY `idcategoria` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `entrevista`
--
ALTER TABLE `entrevista`
  MODIFY `idEntrevista` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT de la tabla `entrevistapormiembro`
--
ALTER TABLE `entrevistapormiembro`
  MODIFY `idEntrevistaPorMiembro` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `huerta`
--
ALTER TABLE `huerta`
  MODIFY `idHuerta` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT de la tabla `huertapormiembro`
--
ALTER TABLE `huertapormiembro`
  MODIFY `idHuertaPorMiembros` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT de la tabla `investigador`
--
ALTER TABLE `investigador`
  MODIFY `idInvestigador` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `miembro`
--
ALTER TABLE `miembro`
  MODIFY `idMiembro` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=61;

--
-- AUTO_INCREMENT de la tabla `opcionrespuesta`
--
ALTER TABLE `opcionrespuesta`
  MODIFY `idOpcionRespuesta` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=191;

--
-- AUTO_INCREMENT de la tabla `pregunta`
--
ALTER TABLE `pregunta`
  MODIFY `idPregunta` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=66;

--
-- AUTO_INCREMENT de la tabla `preguntaporentrevista`
--
ALTER TABLE `preguntaporentrevista`
  MODIFY `idPreguntaPorEntrevista` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=190;

--
-- AUTO_INCREMENT de la tabla `respuestaregistrada`
--
ALTER TABLE `respuestaregistrada`
  MODIFY `idRespuestaRegistrada` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT de la tabla `usuario`
--
ALTER TABLE `usuario`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `entrevista`
--
ALTER TABLE `entrevista`
  ADD CONSTRAINT `fk_Entrevista_Huerta1` FOREIGN KEY (`Huerta_idHuerta`) REFERENCES `huerta` (`idHuerta`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_entrevista_investigador1` FOREIGN KEY (`investigador_idInvestigador`) REFERENCES `investigador` (`idInvestigador`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `entrevistapormiembro`
--
ALTER TABLE `entrevistapormiembro`
  ADD CONSTRAINT `fk_EntrevistaPorMiembro_entrevista1` FOREIGN KEY (`entrevista_idEntrevista`) REFERENCES `entrevista` (`idEntrevista`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_EntrevistaPorMiembro_miembro1` FOREIGN KEY (`miembro_idMiembro`) REFERENCES `miembro` (`idMiembro`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `huertapormiembro`
--
ALTER TABLE `huertapormiembro`
  ADD CONSTRAINT `fk_HuertaPorMiembros_Huerta1` FOREIGN KEY (`Huerta_idHuerta`) REFERENCES `huerta` (`idHuerta`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_HuertaPorMiembros_Miembro1` FOREIGN KEY (`Miembro_idMiembro`) REFERENCES `miembro` (`idMiembro`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `opcionrespuesta`
--
ALTER TABLE `opcionrespuesta`
  ADD CONSTRAINT `fk_OpcionRespuesta_Pregunta1` FOREIGN KEY (`Pregunta_idPregunta`) REFERENCES `pregunta` (`idPregunta`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `pregunta`
--
ALTER TABLE `pregunta`
  ADD CONSTRAINT `fk_pregunta_categoria1` FOREIGN KEY (`categoria_idcategoria`) REFERENCES `categoria` (`idcategoria`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `preguntaporentrevista`
--
ALTER TABLE `preguntaporentrevista`
  ADD CONSTRAINT `fk_PreguntaPorEntrevista_Entrevista1` FOREIGN KEY (`Entrevista_idEntrevista`) REFERENCES `entrevista` (`idEntrevista`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_PreguntaPorEntrevista_Pregunta1` FOREIGN KEY (`Pregunta_idPregunta`) REFERENCES `pregunta` (`idPregunta`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `respuestaregistrada`
--
ALTER TABLE `respuestaregistrada`
  ADD CONSTRAINT `fk_RespuestaRegistrada_OpcionRespuesta1` FOREIGN KEY (`OpcionRespuesta_idOpcionRespuesta`) REFERENCES `opcionrespuesta` (`idOpcionRespuesta`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `pregunta` FOREIGN KEY (`pregunta`) REFERENCES `pregunta` (`idPregunta`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
